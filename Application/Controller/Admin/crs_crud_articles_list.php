<?php
namespace Ewald\crs_Crud\Application\Controller\Admin;

use OxidEsales\Eshop\Application\Controller\Admin\AdminListController;
use OxidEsales\Eshop\Core\Registry;


class crs_crud_articles_list extends \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_majorlistcontroller {


    /**
     * Name of chosen object class (default null).
     *
     * @var string
     */

    protected $_sListClass = 'oxarticle';

    /**
     * Type of list.
     *
     * @var string
     */
    protected $_sListType = 'oxarticlelist';

    protected $_sClass = 'crs_crud_articles_list';
    protected $_blAllowEmptyParentId = true;

    public function render()
    {

        return parent::render();
        //return 'crs_crud_articles_list.tpl';
    }

    /**
     * Schreibt Tabelle in CSV-Datei [TODO: Datei csv.txt zum Download bereitstellen, momentan muss man die Datei separat öffnen.]
     */
    public function csv(){
        parent::Majorcsv();
    }

    /**
     * Diese Funktion gibt die User-Reihenfolge der Spalten wieder (wird entweder aus SQL Tabelle geladen, oder initial standardmäßig geladen).
     */

    public function inputField()
    {
        $sString = ["oxactive", "oxid","oxparentid", "oxartnum", "keinabgleichmitsales", "OXSKIPDISCOUNTS", "OXPRICE", "OXTPRICE", "OXTITLE", "OXTITLE_1", "OXTITLE_2", "OXACTIVEFROM", "OXACTIVETO", "OXEAN", "OXPRICEA", "OXPRICEB", "oxpricec", "oxpriced", "oxpricee", "oxpricef", "oxbprice", "OXUNITNAME", "oxunitquantity", "OXVAT", "OXTHUMB", "OXICON", "oxpic1", "oxpic2", "oxpic3", "oxpic4", "oxpic5", "oxpic6", "oxpic7", "oxpic8", "oxpic9", "oxpic10", "oxpic11", "oxpic12", "OXSEARCHKEYS", "OXSEARCHKEYS_1", "OXSEARCHKEYS_2", "OXWEIGHT", "OXSTOCK", "OXSTOCKFLAG", "OXDELIVERY", "OXINSERT", "OXTIMESTAMP", "OXLENGTH", "OXWIDTH", "OXHEIGHT", "OXISSEARCH", "OXVARNAME", "OXVARNAME_1", "OXVARNAME_2", "OXVARSTOCK", "OXVARCOUNT", "OXVARSELECT", "OXVARSELECT_1", "OXVARSELECT_2", "OXVARMINPRICE", "OXVARMAXPRICE", "OXSHORTDESC", "OXSHORTDESC_1", "OXSHORTDESC_2", "oxsort", "oxsoldamount", "oxnonmaterial", "oxfreeshipping", "OXVENDORID", "OXMANUFACTURERID", "OXRATING", "OXRATINGCNT", "OXMINDELTIME", "OXMAXDELTIME", "OXDELTIMEUNIT", "BODYSHOWPRICE", "bearbeitet", "rabattflag", "IDArtikel", "bnflagbestand", "bestellcode", "keinabgleichmitendkunden", "isplayable", "FlagAngebotFahne", "AngebotFahneText", "AngebotFahneText_1", "AngebotFahneText_2", "Verpackungseinheit"];
        return parent::MajorInputField("ASRArtikel", $sString);
    }

    /**
     * Diese Funktion ist wichtig für den Tabellenaufbau. Abhängig von der Reihenfolge der Spalten und abhängig von
     * eventuellen Suchen wird ein Objekt zurückgegeben, sodass die Tabelle dynamisch erzeugt werden kann.
     */


    public function search(){
        //[TODO:] $sImplode = "'" . implode('","', $arrString) . "'";

        //$sString = ["oxactive", "oxid","oxparentid", "oxartnum", "keinabgleichmitsales", "OXSKIPDISCOUNTS", "OXPRICE", "OXTPRICE", "OXTITLE", "OXTITLE_1", "OXTITLE_2", "OXACTIVEFROM", "OXACTIVETO", "OXEAN", "OXPRICEA", "OXPRICEB", "oxpricec", "oxpriced", "oxpricee", "oxpricef", "oxbprice", "OXUNITNAME", "oxunitquantity", "OXVAT", "OXTHUMB", "OXICON", "oxpic1", "oxpic2", "oxpic3", "oxpic4", "oxpic5", "oxpic6", "oxpic7", "oxpic8", "oxpic9", "oxpic10", "oxpic11", "oxpic12", "OXSEARCHKEYS", "OXSEARCHKEYS_1", "OXSEARCHKEYS_2", "OXWEIGHT", "OXSTOCK", "OXSTOCKFLAG", "OXDELIVERY", "OXINSERT", "OXTIMESTAMP", "OXLENGTH", "OXWIDTH", "OXHEIGHT", "OXISSEARCH", "OXVARNAME", "OXVARNAME_1", "OXVARNAME_2", "OXVARSTOCK", "OXVARCOUNT", "OXVARSELECT", "OXVARSELECT_1", "OXVARSELECT_2", "OXVARMINPRICE", "OXVARMAXPRICE", "OXSHORTDESC", "OXSHORTDESC_1", "OXSHORTDESC_2", "oxsort", "oxsoldamount", "oxnonmaterial", "oxfreeshipping", "OXVENDORID", "OXMANUFACTURERID", "OXRATING", "OXRATINGCNT", "OXMINDELTIME", "OXMAXDELTIME", "OXDELTIMEUNIT", "BODYSHOWPRICE", "bearbeitet", "rabattflag", "__ID_Artikel", "bnflagbestand", "bestellcode", "keinabgleichmitendkunden", "isplayable", "FlagAngebotFahne", "AngebotFahneText", "AngebotFahneText_1", "AngebotFahneText_2", "Verpackungseinheit"];
        $sString = "oxactive,oxid,oxparentid,oxartnum,keinabgleichmitsales,OXSKIPDISCOUNTS,OXPRICE,OXTPRICE,OXTITLE,OXTITLE_1,OXTITLE_2,OXACTIVEFROM,OXACTIVETO,OXEAN,OXPRICEA,OXPRICEB,oxpricec,oxpriced,oxpricee,oxpricef,oxbprice,OXUNITNAME,oxunitquantity,OXVAT,OXTHUMB,OXICON,oxpic1,oxpic2,oxpic3,oxpic4,oxpic5,oxpic6,oxpic7,oxpic8,oxpic9,oxpic10,oxpic11,oxpic12,OXSEARCHKEYS,OXSEARCHKEYS_1,OXSEARCHKEYS_2,OXWEIGHT,OXSTOCK,OXSTOCKFLAG,OXDELIVERY,OXINSERT,OXTIMESTAMP,OXLENGTH,OXWIDTH,OXHEIGHT,OXISSEARCH,OXVARNAME,OXVARNAME_1,OXVARNAME_2,OXVARSTOCK,OXVARCOUNT,OXVARSELECT,OXVARSELECT_1,OXVARSELECT_2,OXVARMINPRICE,OXVARMAXPRICE,OXSHORTDESC,OXSHORTDESC_1,OXSHORTDESC_2,oxsort,oxsoldamount,oxnonmaterial,oxfreeshipping,OXVENDORID,OXMANUFACTURERID,OXRATING,OXRATINGCNT,OXMINDELTIME,OXMAXDELTIME,OXDELTIMEUNIT,BODYSHOWPRICE,bearbeitet,rabattflag,IDArtikel,bnflagbestand,bestellcode,keinabgleichmitendkunden,isplayable,FlagAngebotFahne,AngebotFahneText,AngebotFahneText_1,AngebotFahneText_2,Verpackungseinheit";

        $erg = parent::MajorSearch("oxarticles","ASRArtikel",$sString);

        return $erg;
    }

    /**
     * Diese Funktion wird initial bei Abschicken des forms angesteuert. Sammelt alle nötigen Daten für das SQL-Query und ruft dann
     * eine weitere Funktion auf, entweder löschen oder updaten..
     */

    public function getAll(){
        parent::MajorGetAll("oxarticles");
    }


    public function getToken(){
        return Registry::getConfig()->getRequestParameter('stoken');

    }

    public function getAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    public function replaceAll(){
        parent::MajorReplaceAll("oxarticles");
    }

    /**
     * Diese Funktion schreibt jede Veränderung der Spaltenreihenfolge in die SQL-Tabelle, damit genau diese beim nächsten Login wieder geladen wird.
     */

    public function sortierung(){
        parent::MajorSortierung("ASRArtikel");
    }

    /**
     * Diese Funktion schreibt die Standardreihenfolge der Spalten in die SQL-Tabelle, falls der User das möchte.
     */

    public function sortStand(){
        $sString = "oxactive,oxid,oxparentid,oxartnum,keinabgleichmitsales,OXSKIPDISCOUNTS,OXPRICE,OXTPRICE,OXTITLE,OXTITLE_1,OXTITLE_2,OXACTIVEFROM,OXACTIVETO,OXEAN,OXPRICEA,OXPRICEB,oxpricec,oxpriced,oxpricee,oxpricef,oxbprice,OXUNITNAME,oxunitquantity,OXVAT,OXTHUMB,OXICON,oxpic1,oxpic2,oxpic3,oxpic4,oxpic5,oxpic6,oxpic7,oxpic8,oxpic9,oxpic10,oxpic11,oxpic12,OXSEARCHKEYS,OXSEARCHKEYS_1,OXSEARCHKEYS_2,OXWEIGHT,OXSTOCK,OXSTOCKFLAG,OXDELIVERY,OXINSERT,OXTIMESTAMP,OXLENGTH,OXWIDTH,OXHEIGHT,OXISSEARCH,OXVARNAME,OXVARNAME_1,OXVARNAME_2,OXVARSTOCK,OXVARCOUNT,OXVARSELECT,OXVARSELECT_1,OXVARSELECT_2,OXVARMINPRICE,OXVARMAXPRICE,OXSHORTDESC,OXSHORTDESC_1,OXSHORTDESC_2,oxsort,oxsoldamount,oxnonmaterial,oxfreeshipping,OXVENDORID,OXMANUFACTURERID,OXRATING,OXRATINGCNT,OXMINDELTIME,OXMAXDELTIME,OXDELTIMEUNIT,BODYSHOWPRICE,bearbeitet,rabattflag,IDArtikel,bnflagbestand,bestellcode,keinabgleichmitendkunden,isplayable,FlagAngebotFahne,AngebotFahneText,AngebotFahneText_1,AngebotFahneText_2,Verpackungseinheit";
        parent::MajorSortStand("ASRArtikel",$sString);
    }


    public function SpaltenMin(){
        return parent::MajorSpaltenMin("SpaltenMinArtikel");

    }


    /**
     *  Hier werden die minimierten Spalten mit einem SQL-Query Befehl in die DB gespeichert
     */

    public function saveSpalten(){
        parent::MajorSaveSpalten("SpaltenMinArtikel");

    }

    //[TODO: Finde heraus, was die untenstehenden Funktionen machen :D ]


    protected function _buildSelectString($oListObject = null)
    {
        $Grosseltern = oxNew(AdminListController::class);
        $sQ = $Grosseltern->_buildSelectString($oListObject);

        if ($sQ) {
            $sTable = getViewName("oxarticles");
            // $sQ .= " and $sTable.oxparentid = '' ";

            $sType = false;
            $sArtCat = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("art_category");
            if ($sArtCat && strstr($sArtCat, "@@") !== false) {
                list($sType, $sValue) = explode("@@", $sArtCat);
            }

            switch ($sType) {
                // add category
                case 'cat':
                    $oStr = getStr();
                    $sViewName = getViewName("oxobject2category");
                    $sInsert = "from $sTable left join {$sViewName} on {$sTable}.oxid = {$sViewName}.oxobjectid " .
                        "where {$sViewName}.oxcatnid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue) . " and ";
                    $sQ = $oStr->preg_replace("/from\s+$sTable\s+where/i", $sInsert, $sQ);
                    break;
                // add category
                case 'mnf':
                    $sQ .= " and $sTable.oxmanufacturerid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue);
                    break;
                // add vendor
                case 'vnd':
                    $sQ .= " and $sTable.oxvendorid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue);
                    break;
            }
        }

        return $sQ;
    }

    // Restliche ArticleList-Klasse



    /**
     * Returns array of fields which may be used for product data search
     *
     * @return array
     */
    public function getSearchFields()
    {
        $aSkipFields = ["oxblfixedprice", "oxvarselect", "oxamitemid",
            "oxamtaskid", "oxpixiexport", "oxpixiexported"];
        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);

        return array_diff($oArticle->getFieldNames(), $aSkipFields);
    }

    /**
     * Load category list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return \OxidEsales\Eshop\Application\Model\CategoryList
     */
    public function getCategoryList($sType, $sValue)
    {
        /** @var \OxidEsales\Eshop\Application\Model\CategoryList $oCatTree parent category tree */
        $oCatTree = oxNew(\OxidEsales\Eshop\Application\Model\CategoryList::class);
        $oCatTree->loadList();
        if ($sType === 'cat') {
            foreach ($oCatTree as $oCategory) {
                if ($oCategory->oxcategories__oxid->value == $sValue) {
                    $oCategory->selected = 1;
                    break;
                }
            }
        }

        return $oCatTree;
    }

    /**
     * Load manufacturer list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return oxManufacturerList
     */
    public function getManufacturerList($sType, $sValue)
    {
        $oMnfTree = oxNew(\OxidEsales\Eshop\Application\Model\ManufacturerList::class);
        $oMnfTree->loadManufacturerList();
        if ($sType === 'mnf') {
            foreach ($oMnfTree as $oManufacturer) {
                if ($oManufacturer->oxmanufacturers__oxid->value == $sValue) {
                    $oManufacturer->selected = 1;
                    break;
                }
            }
        }

        return $oMnfTree;
    }

    /**
     * Load vendor list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return oxVendorList
     */
    public function getVendorList($sType, $sValue)
    {
        $oVndTree = oxNew(\OxidEsales\Eshop\Application\Model\VendorList::class);
        $oVndTree->loadVendorList();
        if ($sType === 'vnd') {
            foreach ($oVndTree as $oVendor) {
                if ($oVendor->oxvendor__oxid->value == $sValue) {
                    $oVendor->selected = 1;
                    break;
                }
            }
        }

        return $oVndTree;
    }

    /**
     * Builds and returns array of SQL WHERE conditions.
     *
     * @return array
     */
    public function buildWhere()
    {
        // we override this to select only parent articles
        $this->_aWhere = parent::buildWhere();

        // adding folder check
        $sFolder = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('folder');
        if ($sFolder && $sFolder != '-1') {
            $this->_aWhere[getViewName("oxarticles") . ".oxfolder"] = $sFolder;
        }

        return $this->_aWhere;
    }

    /**
     * Deletes entry from the database
     */
    public function deleteEntry()
    {
        $sOxId = $this->getEditObjectId();
        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        if ($sOxId && $oArticle->load($sOxId)) {
            parent::deleteEntry();
        }
    }





}