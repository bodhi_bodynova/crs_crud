<?php
namespace Ewald\crs_Crud\Application\Controller\Admin;


use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\EshopProfessional\Application\Controller\Admin\AdminListController;
use OxidEsales\Eshop\Core\Registry;


class crs_crud_majorlistcontroller extends \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_adminlistcontroller
{


    public function render()
    {
         parent::render();
        return 'crs_crud_articles_list.tpl';
    }


    /**
     * Schreibt Tabelle in CSV-Datei [TODO: Datei csv.txt zum Download bereitstellen, momentan muss man die Datei separat öffnen.]
     */

    public function Majorcsv(){
        $table = Registry::getConfig()->getRequestRawParameter('csv');
        $dateihandle = fopen(dirname(__DIR__,6)."/export/csv.csv","w");
        fwrite($dateihandle, $table);
        fclose($dateihandle);


        $file = dirname(__DIR__,6)."/export/csv.csv";


        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    /**
     * Diese Funktion gibt die User-Reihenfolge der Spalten wieder (wird entweder aus SQL Tabelle geladen, oder initial standardmäßig geladen).
     * Erwartet wird die Tabelle, die AdminSpaltenReihenfolgeTabelle als Array, und die StandardReihenfolge als Array.
     */
    public function MajorInputField($sASR,$arrRStand){
        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            $query = "SELECT " . $sASR . " FROM oxuser WHERE OXUSERNAME = '" . $email . "'";
            //$query = "SELECT ASRArtikel FROM oxuser WHERE OXUSERNAME = 'c.ewald@bodynova.de'";

            $aFelder = $oDb->getAll($query);

            $arrString = $aFelder[0];
            //$aFelder = json_decode($Felder);
            $arrFelder = explode(',', $arrString[0]);
            //var_dump($arrFelder);
        } catch(\Exception $e){
            echo 'Exception abgefangen', $e->getMessage(), "\n";
        }

        if($arrFelder[0] != '') {
            return $arrFelder;
        } else {
            $arrString = $arrRStand;
            return $arrString;
        }
    }

    /**
     * Diese Funktion ist wichtig für den Tabellenaufbau. Abhängig von der Reihenfolge der Spalten und abhängig von
     * eventuellen Suchen wird ein Objekt zurückgegeben, sodass die Tabelle dynamisch erzeugt werden kann.
     *
     * Es muss übergeben werden : "AdminSpaltenReihenfolgeTabelle", "Tabelle", "StandardReihenfolgeQuery"...
     */

    public function MajorSearch($sTable,$sASR,$sRStandQuery){
      //  echo($sRStandQuery);
      /*  $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);

        $query = " SELECT " . $sRStandQuery ." FROM " . $sTable;
        echo($query);
        $erg = $oDb->getAll($query);
        print_r($erg);
        die(); */
        $aufab = Registry::getConfig()->getRequestRawParameter('aufab');

        $ascdesc = Registry::getConfig()->getRequestRawParameter('asc');

        //$this->asc[0] = $ascdesc;

        if($aufab == null){
            $aufab = 'OXID';
        }

        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;

        // var_dump($arrFelder);
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);

            $query = "SELECT " . $sASR . " FROM oxuser WHERE OXUSERNAME = '" . $email . "'";
            //$query = "SELECT ASRArtikel FROM oxuser WHERE OXUSERNAME = 'c.ewald@bodynova.de'";
            $aFelder = $oDb->getAll($query);
            $sString = $aFelder[0];
            //$aFelder = json_decode($Felder);
            $arrFelder = explode(',', $sString[0]);
            //var_dump($arrFelder);

        } catch(\Exception $e){
            echo 'Exception abgefangen', $e->getMessage(), "\n";
        }

        $Anzahl = Registry::getConfig()->getRequestRawParameter('AnzahlSuche');

        $suche1 = Registry::getConfig()->getRequestRawParameter('suche1');
        $Spalte1 = Registry::getConfig()->getRequestRawParameter('1');
        $suche1 = str_replace(' ','%',$suche1);


        $sLike = "LIKE";
        $sProzent = "%";

        if($suche1[0] == "'" && $suche1[strlen($suche1)-1] == "'"){
            $suche1 = substr($suche1,1,-1);
            $sLike = "=";
            $sProzent = null;
            }


        if($arrFelder[0] != ''){

            $sString = $aFelder[0];
            $arrFelder = explode(',',$sString[0]);
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);

            if(empty($Spalte1)) {
                $Spalte1 = '""';
            } else {
                if(!empty($suche1)){
                    //$queryTable = "SELECT " . $sString[0] . " FROM " . $sTable . "  WHERE " . $Spalte1 ." LIKE '%" . $suche1 . "%'";
                    $queryTable = "SELECT " . $sString[0] . " FROM " . $sTable . "  WHERE " . $Spalte1 ." " . $sLike . " '$sProzent" . $suche1 . "$sProzent'";
                } else {
                    $suche1 = '""';
                }
            }
            ${variable} = 'Suche';
            ${spalte} = 'Spalte';

            for($i=2;$i<=$Anzahl;$i++){
                ${variable.$i} = Registry::getConfig()->getRequestRawParameter("suche$i");
                ${variable.$i} = str_replace(' ','%',${variable.$i});
                ${spalte.$i} = Registry::getConfig()->getRequestRawParameter("$i");
                if(empty(${spalte.$i})) {
                    if(!$queryTable){

                    }
                    ${spalte.$i} = '""';
                } else {
                    if(!empty(${variable.$i})){
                        if(!$queryTable){
                            $queryTable = "SELECT " . $sString[0] . " FROM " . $sTable . " WHERE " . ${spalte.$i} . " LIKE '%" . ${variable.$i} . "%'";
                        } else {
                            $queryTable .= " OR " . ${spalte.$i} . " LIKE '%" . ${variable.$i}."%'";
                        }
                    } else {
                        ${variable.$i} = '""';
                    }
                }
            }

            if(!$queryTable){
                $queryTable = "SELECT " . $sString[0] . " FROM " . $sTable;
            }

            $queryTable .= " ORDER BY " . $aufab . " " . $ascdesc;

            if($sTable == 'oxuser'){
                $queryTable .= " LIMIT 1000";
            } else {
                $queryTable .= " LIMIT 10000";
            }

            try{
                $erg = $oDb->getAll($queryTable);
            } catch(\Exception $e){
                return 'Wenn du eine genaue Suche haben möchtest, bitte auch nur nach einem Wert suchen. Exception abgefangen: ' . $e->getMessage() . "\n";
            }


            return $erg;
        } else {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            if(empty($Spalte1)) {
                $Spalte1 = '""';
            }
            $query = " SELECT " . $sRStandQuery ." FROM " . $sTable . " WHERE " . $Spalte1 ." LIKE '%" . $suche1 . "%'";

            //print_r($query);
            ${variable} = 'Suche';
            ${spalte} = 'Spalte';

            for($i=2;$i<=$Anzahl;$i++){
                ${variable.$i} = Registry::getConfig()->getRequestRawParameter("suche$i");
                ${variable.$i} = str_replace(' ','%',${variable.$i});
                ${spalte.$i} = Registry::getConfig()->getRequestRawParameter("$i");
                if(empty(${spalte.$i})) {
                    ${spalte.$i} = '""';
                }
                $query .= " OR " .${spalte.$i} . " LIKE '%" . ${variable.$i} . "%'";
            }
            $query .= " ORDER BY " . $aufab . " " . $ascdesc;
            if($sTable == 'oxuser'){
                $query .= " LIMIT 1000";
            } else {
                $query .= " LIMIT 10000";
            }

            try{
                $erg = $oDb->getAll($query);
            } catch(\Exception $e){
                return 'Wenn du eine genaue Suche haben möchtest, bitte auch nur nach einem Wert suchen. Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            return $erg;
        }
    }

    /**
     * Diese Funktion wird initial bei Abschicken des forms angesteuert. Sammelt alle nötigen Daten für das SQL-Query und ruft dann
     * eine weitere Funktion auf, entweder löschen oder updaten..
     */

    public function MajorGetAll($sTable){

        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;

        $oxid = Registry::getConfig()->getRequestRawParameter('oxid');
        $text = Registry::getConfig()->getRequestRawParameter('text');
        $Spalte = Registry::getConfig()->getRequestRawParameter('Spalte');
        $delete = Registry::getConfig()->getRequestRawParameter('delete');
        $recover = Registry::getConfig()->getRequestRawParameter('recover');



        if($delete == true){
            $this->loesche($oxid,$email,$sTable);
            return;
        }
        $this->update($oxid,$text,$Spalte,$email,$recover,$sTable);
        return;
    }


    public function MajorGetToken(){
        return Registry::getConfig()->getRequestParameter('stoken');

    }

    public function MajorGetAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    /**
     * Hier wird ein Eintrag aus der Datenbank gelöscht. Mit Hilfe der OXID ist eine genaue Zuweisung möglich.
     * Außerdem wird zusätzlich in die Log-File geschrieben mit der zugehörigen Aktion und welcher User diese vorgenommen hat.
     */

    public function loesche($oxid,$email,$sTable){
        try {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

            $query = "DELETE FROM " . $sTable . " WHERE OXID = ?";
            $querySelect = "SELECT * FROM " . $sTable . " WHERE OXID = '" . $oxid . "'";

            $arrUpdateRequest = array($oxid);

            $SelectQuery = $oDb->getAll($querySelect);
            $InsertQuery = "INSERT INTO " . $sTable . " VALUES ('";
            foreach($SelectQuery[0] as &$value){
                $InsertQuery .= $value . "','";
            }
            $InsertQuery = substr($InsertQuery, 0, -2);
            $InsertQuery .= ");";

            $UpdateRequest = $oDb->execute($query,$arrUpdateRequest);


            $date = date("Y-m-d H:i:s");
            $log = $date . ' --- User: ' . $email .'     Aktion: DELETE FROM ' . $sTable . ' WHERE OXID = ' . $oxid . ".    UNDO-SQL: " . $InsertQuery . "\n" ;
            $dateihandle = fopen(dirname(__DIR__,3)."/log/changes.txt","a");
            fwrite($dateihandle, $log);
            fclose($dateihandle);

        } catch(\Exception $e){
            echo 'Exception abgefangen: ' . $querySelect, $e->getMessage(), "\n";
        }

        //echo json_encode($UpdateRequest);
        return;
    }

    /**
     * Hier wird ein Eintrag aktualisiert bzw. verändert. Hier benötigen wir mehrere Variablen, damit wir sichergehen können,
     * dass der richtige Eintrag geändert wird. Spalte,Text,OXID ist wichtig für das Query,
     * Email und Recover für die Logfile, damit wir den vorherigen Wert im Notfall einsehen können.
     */

    public function update($oxid,$text,$Spalte,$email,$recover,$sTable){

        //echo json_encode($query); //"Erfolg: " .$query;
        //die();

        try {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);

            $query = "UPDATE " . $sTable . " SET " . $Spalte . " = ? WHERE OXID = ?";

            $arrUpdateRequest = array($text,$oxid);

            $UpdateRequest = $oDb->execute($query,$arrUpdateRequest);

            $date = date("Y-m-d H:i:s");
            $log = $date . ' --- User: ' . $email .'     Aktion: UPDATE ' . $sTable . ' SET ' . $Spalte . ' = ' . $text . ' WHERE OXID = ' . $oxid . '.      VORHERIGER WERT: ' . $recover . "\n" ;

            $dateihandle = fopen(dirname(__DIR__,3)."/log/changes.txt","a");
            fwrite($dateihandle, $log);
            fclose($dateihandle);

        } catch(\Exception $e){
            echo 'Exception abgefangen: ', $e->getMessage(), "\n" ;

        }
        //echo json_encode($UpdateRequest);
        return ;

    }

    /**
     * Eine Replace-All Funktion, die nach Filtern alle gezeigten Einträge bearbeitet!
     * Gleichzeitig wird in die Log-Datei geschrieben, welche Werte zuvor eingetragen waren.
     * Außerdem gibt es ein Recover-Query, das man einfach rauskopieren kann, um es zB. in Navicat abzufeuern.
     */

    public function MajorReplaceAll($sTable){

        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;

        $text = Registry::getConfig()->getRequestRawParameter('text');
        $arr = Registry::getConfig()->getRequestRawParameter('array');
        $spalte = Registry::getConfig()->getRequestRawParameter('spalte');

        $recover = "SELECT " . $spalte . " FROM " . $sTable . " WHERE OXID = '" . $arr[0] . "'";


        try {
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $oDbNum = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);

            $query = "UPDATE " . $sTable . " SET " . $spalte . " ='" . $text . "' WHERE OXID = '" . $arr[0] . "'";
            for($i=1;$i<sizeof($arr);$i++){
                if($arr[$i] != '') {
                    $query .= " OR OXID = '" . $arr[$i] . "'";
                    $recover .= " OR OXID ='" . $arr[$i] . "'";
                }
            }
            $ergRecover = $oDbNum->getAll($recover);
            $erg = $oDb->execute($query);
            $date = date("Y-m-d H:i:s");
            $sRecover ="";
            $sRecoverSQLquery = "";
            for($j=0;$j<sizeof($ergRecover);$j++){

                $test = $ergRecover[$j];
                $sRecover .= " " . $arr[$j] . " ->: " . $test[0] . ";";
                $sRecoverSQLquery .= "UPDATE " . $sTable . " SET " . $spalte . " = '" . $test[0] . "' WHERE OXID = '" . $arr[$j] . "';";
            }
            $log = $date . ' --- User: ' . $email .'     Aktion: ' . $query . '.      VORHERIGER WERT: ' . $sRecover . ' . SQL-Query zum zurücksetzen: ' . $sRecoverSQLquery . "\n";
            $dateihandle = fopen(dirname(__DIR__,3)."/log/changes.txt","a");
            fwrite($dateihandle, $log);
            fclose($dateihandle);

        } catch(\Exception $e){
            echo 'Exception abgefangen: ', $e->getMessage(), "\n";
        }
        return ;
    }

    /**
     * Diese Funktion schreibt jede Veränderung der Spaltenreihenfolge in die SQL-Tabelle, damit genau diese beim nächsten Login wieder geladen wird.
     */

    public function MajorSortierung($sASR){
        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        $sort = Registry::getConfig()->getRequestRawParameter('query');
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            $query = "UPDATE oxuser SET " . $sASR . " = '" . $sort . "' WHERE OXUSERNAME = '" . $email . "'";
            $oDb->execute($query);
        } catch(\Exception $e){
            echo 'Exception abgefangen: ', $e->getMessage(), "\n";
        }
        return;
    }

    /**
     * Diese Funktion schreibt die Standardreihenfolge der Spalten in die SQL-Tabelle, falls der User das möchte.
     */

    public function MajorSortStand($sASR,$sRStandQuery){
        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            $query = "UPDATE oxuser SET " . $sASR . " = '" . $sRStandQuery . "' WHERE OXUSERNAME = '" . $email . "'";

            $oDb->execute($query);

        } catch(\Exception $e){
            return 'Exception abgefangen' .  $e->getMessage() . "\n";
        }
        return;
    }

    /**
     * Diese Funktion liest die minimierten Spalten aus der DB aus.
     */

    public function MajorSpaltenMin($sString){
        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        try{
            $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            $query = "SELECT " . $sString . "  FROM oxuser WHERE OXUSERNAME = '" . $email . "'";
            //$query = "SELECT ASRArtikel FROM oxuser WHERE OXUSERNAME = 'c.ewald@bodynova.de'";

            $aFelder = $oDb->getAll($query);

            $arrString = $aFelder[0];
            //$aFelder = json_decode($Felder);
            $arrFelder = explode(',', $arrString[0]);
            //var_dump($arrFelder);
        } catch(\Exception $e){
            echo 'Exception abgefangen', $e->getMessage(), "\n";
        }

        if($arrFelder[0] != '') {
            return $arrFelder;
        }
    }

    /**
     * Diese Funktion schreibt die minimeirten Spalten in die DB.
     */

    public function MajorSaveSpalten($sString){
        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        $arr = Registry::getConfig()->getRequestRawParameter('array');
        echo '<pre>';
        $query = "UPDATE oxuser SET " . $sString . " = '";
        for($i = 0; $i<sizeof($arr)-1;$i++){
            $query .= $arr[$i] . ",";
        }
        $query .= $arr[sizeof($arr)-1] . "' WHERE oxusername = '" . $email . "'";
        try{
            $Odb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);
            return $Odb->execute($query);
        } catch(\Exception $e){
            echo 'Exception abgefangen: ' . $e->getMessage() . "\n";
        }
        return;

    }

    /**
     * Builds and returns SQL query string.
     *
     * @param object $oListObject list main object
     *
     * @return string
     */






    protected function _buildSelectString($oListObject = null)
    {


        $Grosseltern = oxNew(AdminListController::class);
        $sQ = $Grosseltern->_buildSelectString($oListObject);



        if ($sQ) {
            $sTable = getViewName("oxarticles");
            // $sQ .= " and $sTable.oxparentid = '' ";

            $sType = false;
            $sArtCat = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("art_category");
            if ($sArtCat && strstr($sArtCat, "@@") !== false) {
                list($sType, $sValue) = explode("@@", $sArtCat);
            }

            switch ($sType) {
                // add category
                case 'cat':
                    $oStr = getStr();
                    $sViewName = getViewName("oxobject2category");
                    $sInsert = "from $sTable left join {$sViewName} on {$sTable}.oxid = {$sViewName}.oxobjectid " .
                        "where {$sViewName}.oxcatnid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue) . " and ";
                    $sQ = $oStr->preg_replace("/from\s+$sTable\s+where/i", $sInsert, $sQ);
                    break;
                // add category
                case 'mnf':
                    $sQ .= " and $sTable.oxmanufacturerid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue);
                    break;
                // add vendor
                case 'vnd':
                    $sQ .= " and $sTable.oxvendorid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue);
                    break;
            }
        }

        return $sQ;
    }

    // Restliche ArticleList-Klasse



    /**
     * Returns array of fields which may be used for product data search
     *
     * @return array
     */
    public function getSearchFields()
    {
        $aSkipFields = ["oxblfixedprice", "oxvarselect", "oxamitemid",
            "oxamtaskid", "oxpixiexport", "oxpixiexported"];
        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);

        return array_diff($oArticle->getFieldNames(), $aSkipFields);
    }

    /**
     * Load category list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return \OxidEsales\Eshop\Application\Model\CategoryList
     */
    public function getCategoryList($sType, $sValue)
    {
        /** @var \OxidEsales\Eshop\Application\Model\CategoryList $oCatTree parent category tree */
        $oCatTree = oxNew(\OxidEsales\Eshop\Application\Model\CategoryList::class);
        $oCatTree->loadList();
        if ($sType === 'cat') {
            foreach ($oCatTree as $oCategory) {
                if ($oCategory->oxcategories__oxid->value == $sValue) {
                    $oCategory->selected = 1;
                    break;
                }
            }
        }

        return $oCatTree;
    }

    /**
     * Load manufacturer list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return oxManufacturerList
     */
    public function getManufacturerList($sType, $sValue)
    {
        $oMnfTree = oxNew(\OxidEsales\Eshop\Application\Model\ManufacturerList::class);
        $oMnfTree->loadManufacturerList();
        if ($sType === 'mnf') {
            foreach ($oMnfTree as $oManufacturer) {
                if ($oManufacturer->oxmanufacturers__oxid->value == $sValue) {
                    $oManufacturer->selected = 1;
                    break;
                }
            }
        }

        return $oMnfTree;
    }

    /**
     * Load vendor list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return oxVendorList
     */
    public function getVendorList($sType, $sValue)
    {
        $oVndTree = oxNew(\OxidEsales\Eshop\Application\Model\VendorList::class);
        $oVndTree->loadVendorList();
        if ($sType === 'vnd') {
            foreach ($oVndTree as $oVendor) {
                if ($oVendor->oxvendor__oxid->value == $sValue) {
                    $oVendor->selected = 1;
                    break;
                }
            }
        }

        return $oVndTree;
    }

    /**
     * Builds and returns array of SQL WHERE conditions.
     *
     * @return array
     */
    public function buildWhere()
    {
        // we override this to select only parent articles
        $this->_aWhere = parent::buildWhere();

        // adding folder check
        $sFolder = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('folder');
        if ($sFolder && $sFolder != '-1') {
            $this->_aWhere[getViewName("oxarticles") . ".oxfolder"] = $sFolder;
        }

        return $this->_aWhere;
    }

    /**
     * Deletes entry from the database
     */
    public function deleteEntry()
    {
        $sOxId = $this->getEditObjectId();
        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        if ($sOxId && $oArticle->load($sOxId)) {
            parent::deleteEntry();
        }
    }

    public function AdminZeilen()
    {

    }





}