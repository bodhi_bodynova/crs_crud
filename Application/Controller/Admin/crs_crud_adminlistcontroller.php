<?php

namespace Ewald\crs_Crud\Application\Controller\Admin;

use Bender\dre_BnFlowExtends\Core\Exception\Exception;
use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Core\Oxid;
use OxidEsales\Eshop\Core;
use OxidEsales\EshopProfessional\Application\Controller\Admin\AdminListController;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\Request;

use oxRegistry;
use oxDb;
use oxField;
use stdClass;
use oxList;
use oxBase;
use oxI18n;
use Ewald\crs_Crud\Application\Controller\Admin\crs_crud_articles_list;

class crs_crud_adminlistcontroller extends \OxidEsales\Eshop\Application\Controller\Admin\AdminListController
{

    protected $asc = null;

    protected $suche = null;

    protected $spalte = null;

    protected $anzahl = null;

    protected $anzahlErgebnisse = null;

    protected $aufab = null;

    protected $actPage = null;

    protected $styleTA = null;

    public function getAsc(){

        return $this->asc;
    }

    public function getSuche(){
        return $this->suche;
    }

    public function getSpalte(){
        return $this->spalte;
    }

    public function getAnzahl(){
        return $this->anzahl;
    }

    public function getAnzahlErgebnisse(){
        return $this->anzahlErgebnisse;
    }
    public function getAufAb(){
        return $this->aufab;
    }

    public function getActPage(){
        return $this->actPage;
    }

    public function getStyleTA(){
        return $this->styleTA;
    }

    public function AdminZeilen()
    {

    }

    /**
     * [TODO: fertige Funktion zur Verarbeitung der Suchparameter an. Da es mehrere geben kann, iteriere durch ein Array (AnzahlSuche als Iteration)]
     */


    protected function _setListNavigationParams()
    {

        //getStyle for Textarea (Größe des Eingabefeldes)

        $styleTA = Registry::getConfig()->getRequestRawParameter('styleTA');
        $this->styleTA = $styleTA;

        $ascdesc = Registry::getConfig()->getRequestRawParameter('asc');
        $this->asc[0] = $ascdesc;



        // list navigation
        $showNavigation = false;
        $adminListSize = $this->_getViewListSize();


        /**
         * pageNavigation->pages wird überschrieben mit Hilfe des querys, was zur Suche aufgebaut wird.
         */

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);



        $Anzahl1 = Registry::getConfig()->getRequestRawParameter('AnzahlSuche');


        $suche1 = Registry::getConfig()->getRequestRawParameter('suche1');
        $Spalte1 = Registry::getConfig()->getRequestRawParameter('1');

        $actPage = Registry::getConfig()->getRequestRawParameter('actPage');
        $this->actPage[0] = intval($actPage);

        //echo( 'actPage:    '   .   $actPage . "<br>");


        //echo('Suche:  ' . $suche1 . '    und Spalte: ' . $Spalte1 . "<br>");
        $this->anzahl[0] = $Anzahl1;

        $suche1 = str_replace(' ','%',$suche1);
        $sProzent ="%";
        $sLike ="LIKE";
        if($suche1[0] == "'" && $suche1[strlen($suche1)-1] == "'"){
            $this->suche[0] = $suche1;
            $suche1 = substr($suche1,1,-1);
            $sLike = "=";
            $sProzent = null;
        }

        if(empty($Spalte1)) {
            $Spalte1 = '""';
        } else{
            if(!empty($suche1)){
                $queryCount = "SELECT COUNT('oxid') FROM oxarticles  WHERE " . $Spalte1 ." $sLike '$sProzent" . $suche1 . "$sProzent'";

                $this->suche[0] = $suche1;
                /* // 1. Suchergebnis sollte immer gespeichert werden!?
                if($suche1 == '""') {
                    $this->suche[0] = $suche1;
                }*/

                $this->spalte[0] = $Spalte1;
            } else {
                $suche1 = '""';
            }
        }

        //echo($queryCount . "<br>");



        ${variable} = 'Suche';
        ${spalte} = 'Spalte';

        for($i=2;$i<=$Anzahl1;$i++){

            ${variable.$i} = Registry::getConfig()->getRequestRawParameter("suche$i");
            ${variable.$i} = str_replace(' ','%',${variable.$i});
            ${spalte.$i} = Registry::getConfig()->getRequestRawParameter("$i");




            if(empty(${spalte.$i})) {
                if(!$queryCount){
                    //$queryCount = "SELECT COUNT('oxid') FROM oxarticles WHERE " . ${variable.$i} ." LIKE '%" . ${spalte.$i} . "%'";
                }
                ${spalte.$i} = '""';
            } else{
                if(!empty(${variable.$i})) {
                    if(!$queryCount){
                        $queryCount = "SELECT COUNT('oxid') FROM oxarticles WHERE " . ${spalte.$i} ." LIKE '%" . ${variable.$i} . "%'";
                    } else {
                        $queryCount .= " OR " . ${spalte.$i} . " LIKE '%" . ${variable.$i} . "%'";
                    }
                    $this->suche[$i-1] = ${variable.$i};
                    $this->spalte[$i-1] = ${spalte.$i};
                } else {
                    ${variable.$i} = '""';
                }
            }

            //echo($queryCount);


            // $queryCount .= " OR " .${spalte.$i} . " LIKE '%" . ${variable.$i} . "%'";

            //echo(' Suche: ' . ${variable.$i} . ' und Spalte: ' . ${spalte.$i} . "<br>");
        }

        if(!$queryCount){
            $queryCount = "SELECT COUNT('oxid') FROM oxarticles";
        }
        try{
            $zahl = $oDb->getOne($queryCount);
        } catch(\Exception $e){
            return 'Wenn du eine genaue Suche haben möchtest, bitte auch nur nach einem Wert suchen. Exception abgefangen: ' . $e->getMessage() . "\n";
        }

        if($zahl > 10000){
            $zahl = 10000;
        }

        //echo($queryCount . '       Anzahl:    ' . $zahl);
        /**
         *
         */
        $AnzahlErgebnisse = Registry::getConfig()->getRequestRawParameter('AnzahlErgebnisse');

        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        try{
            $oDbN = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            $queryAdminZeilen = "SELECT AdminZeilen FROM `oxuser` WHERE OXUSERNAME = '" .$email ."'";

            $AnzahlErgebnisse = $oDbN->getAll($queryAdminZeilen);
            $sVar = $AnzahlErgebnisse[0];
        } catch(\Exception $e){
            return 'Exception abgefangen: ' . $e->getMessage() . "\n";
        }



        if($sVar[0] == ''){
            $sVar[0] = 10;
            $this->anzahlErgebnisse[0] = $sVar[0];
        }
        $this->anzahlErgebnisse[0] = $sVar[0];
        if ($this->_iListSize > $adminListSize) {
            // yes, we need to build the navigation object
            $pageNavigation = new stdClass();

            $pageNavigation->pages = round((($zahl) / $sVar[0]) + 0.4999999, 0);

            $pageNavigation->actpage = $actPage+1;


            $pageNavigation->lastlink = ($pageNavigation->pages - 1) * $adminListSize;
            $pageNavigation->nextlink = null;
            $pageNavigation->backlink = null;

            $position = $this->_iCurrListPos + $adminListSize;
            if ($position < $this->_iListSize) {
                $pageNavigation->nextlink = $position = $this->_iCurrListPos + $adminListSize;
            }

            if (($this->_iCurrListPos - $adminListSize) >= 0) {
                $pageNavigation->backlink = $position = $this->_iCurrListPos - $adminListSize;
            }

            // calculating list start position
            $start = $pageNavigation->actpage - 5;
            $start = ($start <= 0) ? 1 : $start;

            // calculating list end position
            $end = $pageNavigation->actpage + 5;
            $end = ($end < $start + 10) ? $start + 10 : $end;
            $end = ($end > $pageNavigation->pages) ? $pageNavigation->pages : $end;

            // once again adjusting start pos ..
            $start = ($end - 10 > 0) ? $end - 10 : $start;
            $start = ($pageNavigation->pages <= 11) ? 1 : $start;

            // navigation urls
            for ($i = $start; $i <= $end; $i++) {
                $page = new stdclass();
                $page->selected = 0;
                if ($i == $pageNavigation->actpage) {
                    $page->selected = 1;
                }
                $pageNavigation->changePage[$i] = $page;
            }

            $this->_aViewData['pagenavi'] = $pageNavigation;

            if (isset($this->_iOverPos)) {
                $position = $this->_iOverPos;
                $this->_iOverPos = null;
            } else {
                $position = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('lstrt');
            }

            if (!$position) {
                $position = 0;
            }

            $this->_aViewData['lstrt'] = $position;
            $this->_aViewData['listsize'] = $this->_iListSize;
            $showNavigation = true;
        }

        // determine not used space in List
        $listSizeToShow = $this->_iListSize - $this->_iCurrListPos;
        $adminListSize = $this->_getViewListSize();
        $notUsed = $adminListSize - min($listSizeToShow, $adminListSize);
        $space = $notUsed * 15;


        if (!$showNavigation) {
            $space += 20;
        }

        $this->_aViewData['iListFillsize'] = $space;
    }

    /**
     * Calculates list items count
     *
     * @param string $sql SQL query used co select list items
     */
    protected function _calcListItemsCount($sql)
    {
        $stringModifier = getStr();
        // count SQL
        $sql = $stringModifier->preg_replace('/select .* from/i', 'select count(*) from ', $sql);

        // removing order by
        $sql = $stringModifier->preg_replace('/order by .*$/i', '', $sql);

        // We force reading from master to prevent issues with slow replications or open transactions (see ESDEV-3804).
        // con of list items which fits current search conditions
        $this->_iListSize = \OxidEsales\Eshop\Core\DatabaseProvider::getMaster()->getOne($sql);

        // set it into session that other frames know about size of DB
        \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('iArtCnt', $this->_iListSize);
    }

    public function getItemList()
    {
        if ($this->_oList === null && $this->_sListClass) {
            $this->_oList = oxNew($this->_sListType);
            $this->_oList->clear();
            $this->_oList->init($this->_sListClass);

            $where = $this->buildWhere();

            $listObject = $this->_oList->getBaseObject();

            \OxidEsales\Eshop\Core\Registry::getSession()->setVariable('tabelle', $this->_sListClass);
            $this->_aViewData['listTable'] = getViewName($listObject->getCoreTableName());
            $this->getConfig()->setGlobalParameter('ListCoreTable', $listObject->getCoreTableName());

            if ($listObject->isMultilang()) {
                // is the object multilingual?
                /** @var \OxidEsales\Eshop\Core\Model\MultiLanguageModel $listObject */
                $listObject->setLanguage(\OxidEsales\Eshop\Core\Registry::getLang()->getBaseLanguage());

                if (isset($this->_blEmployMultilanguage)) {
                    $listObject->setEnableMultilang($this->_blEmployMultilanguage);
                }
            }

            $query = $this->_buildSelectString($listObject);
            $query = $this->_prepareWhereQuery($where, $query);
            $query = $this->_prepareOrderByQuery($query);
            $query = $this->_changeselect($query);



            // calculates count of list items
            $this->_calcListItemsCount($query);

            // setting current list position (page)
            $this->_setCurrentListPosition(\OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('jumppage'));

            // setting addition params for list: current list size
            $this->_oList->setSqlLimit($this->_iCurrListPos, $this->_getViewListSize());

            $this->_oList->selectString($query);
        }

        return $this->_oList;
    }

    /**
     * Viewable list size getter
     *
     * @return int
     */
    protected function _getViewListSize()
    {
        $aufab = Registry::getConfig()->getRequestRawParameter('aufab');
        $this->aufab = $aufab;

        $oDbN = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);

        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        try{
            $queryAdminZeilen = "SELECT AdminZeilen FROM `oxuser` WHERE OXUSERNAME = '" .$email ."'";
            $AnzahlErgebnisse = $oDbN->getAll($queryAdminZeilen);
            $sVar = $AnzahlErgebnisse[0];
        } catch(\Exception $e){
            return 'Exception abgefangen: ' . $e->getMessage() . "\n";
        }
        $AnzahlInput = Registry::getConfig()->getRequestRawParameter('AnzahlErgebnisse');
        if($AnzahlInput == null){
            $AnzahlInput = $sVar[0];
        }
        if($AnzahlInput != $sVar[0] ){
            $sVar[0] = $AnzahlInput;

            try{
                $query = "UPDATE oxuser SET AdminZeilen = '" . $AnzahlInput . "' WHERE OXUSERNAME = '" . $email . "'";
                $oDbN->execute($query);
            } catch(\Exception $e){
                return 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }
        }

        if($sVar[0] == ''){
            $sVar[0] = 10;
        }
        if (!$this->_iViewListSize) {
            $config = $this->getConfig();
            if ($profile = \OxidEsales\Eshop\Core\Registry::getSession()->getVariable('profile')) {
                if (isset($profile[1])) {
                    $config->setConfigParam('iAdminListSize', (int)$profile[1]);
                }
            }

            $this->_iViewListSize = $sVar[0];//(int)$config->getConfigParam('iAdminListSize');
            if (!$this->_iViewListSize) {
                $this->_iViewListSize = $sVar[0];
                $config->setConfigParam('iAdminListSize', $this->_iViewListSize);
            }
        }

        return $this->_iViewListSize;
    }
}