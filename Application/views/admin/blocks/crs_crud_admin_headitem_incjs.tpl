[{$smarty.block.parent}]

[{if $oView->getClassName() == 'crs_crud_articles_list'
|| $oView->getClassName() == 'crs_crud_user_list'
|| $oView->getClassName() == 'crs_crud_actions2article_list'
|| $oView->getClassName() == 'crs_crud_actions_list'
|| $oView->getClassName() == 'crs_crud_artextends_list'
|| $oView->getClassName() == 'crs_crud_attribute_list'
|| $oView->getClassName() == 'crs_crud_categories_list'
|| $oView->getClassName() == 'crs_crud_contents_list'
|| $oView->getClassName() == 'crs_crud_delivery_list'
|| $oView->getClassName() == 'crs_crud_deliveryset_list'
|| $oView->getClassName() == 'crs_crud_object2select_list'
|| $oView->getClassName() == 'crs_crud_order_list'
|| $oView->getClassName() == 'crs_crud_price2article_list'
|| $oView->getClassName() == 'crs_crud_select_list'
}]
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>


    <script type="text/javascript" src="[{$oViewConf->getModuleUrl('crs_crud','out/src/js/bootstrap.min.js')}]" ></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.js" ></script>
    <script src=https://legacy.datatables.net/extras/thirdparty/ColReorderWithResize/ColReorderWithResize.js></script>

    <script type="text/javascript" src="[{$oViewConf->getModuleUrl('crs_crud','out/src/js/drag.js')}]"></script>






    <script type="text/javascript">

        /**
         *  function enlarge, function saveSort
         *  Funktionen zur Bearbeitung der Spaltenreihenfolge. Der User kann die Spaltenreihenfolge beliebig verändern und abschließend per Klick in der Datenbank speichern.
         */

        $( function() {
            $( "#sortable" ).sortable(/*{
                stop: function(){
                    var string = '';
                    var x = $('#sortable');
                    console.log($('#sortable'));
                    for(var i = 0; i<x[0].children.length; i++){
                        string += x[0].children[i].id + ',';
                    }
                    console.log(string);
                }
            }*/);
            $( "#sortable" ).disableSelection();
        } );

        function enlarge(){
            if($('#containerSort').hasClass('hidden')){
                $('#containerSort').removeClass('hidden');
            } else {
                $('#containerSort').addClass('hidden');
            }
        }

        function saveSort(){
            $('#containerSort').addClass('hidden');
            var Sortierung = '';
            var x = $('#sortable');
            for(var i = 0; i<x[0].children.length; i++){
                Sortierung += x[0].children[i].id + ',';
            }
            Sortierung = Sortierung.substr(0,Sortierung.length-1);
            console.log(Sortierung);
            $.ajax({
                type: 'POST',

                url: 'index.php?cl=[{$oView->getClassName()}]&fnc=sortierung&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
                data: {
                    query: Sortierung
                },
                success: function(result){
                    console.log('klappt');
                    location.reload();
                },
                error: function(result){
                    console.log('\nERROR');
                }
            });
        }

        /**
         * Popover-Funktionalitäten, Ausgabe des Inhalts von <a .. ></a> in ein hidden Input-Field..
         */

        (function ($, window, document) {

            $(function () {
                // Now, DOM is ready and we can perform DOM functions.
                /*  var table = $('#defaultTable').DataTable({
                      'dom': 'Rlfrtip',
                      'colReorder': {
                          'allowReorder': false
                      },
                      'ordering' : false,
                      paging:false
                  }); */

                $('.popover-dismiss').popover({
                    trigger: "manual",
                    html: true,

                    content: function(){
                        return $(this).parent().children('.content').html();
                    }
                });

                $('[data-toggle=popover]').on('click', function (e) {
                    $('[data-toggle=popover]').not(this).popover('hide');
                });

                $('.popover-dismiss').on({
                    "shown.bs.popover": function(){
                    },
                    "hide.bs.popover": function(){
                        $(this).blur();
                    },
                    "click": function(){
                        $(this).popover("toggle");
                    }
                });

            });
        }(window.jQuery, window, document));


        /**
         * Informationen bei Drag$Drop werden gesammelt, um nachher die Reihenfolge zu speichern.
         */

        // TODO: stoken und admin-sid variabel lassen





        /**
         * Funktion zum speichern, welche Spalten minimiert sind. Die Werte werden als 0 und 1 in die DB geschrieben. Bei erneutem Laden wird aus der PHP-Datei
         * der D
         */

        window.onload = function () {

            top.reloadEditFrame();
            [{if $updatelist == 1}]
                // top.oxid.admin.updateList('[{$oxid}]');
                [{/if}]

        };

        $(document).ready(function(){
            $('#spinner').css("display","none");

            $(window).keydown(function(event){
                if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });


            console.log($("input[name='sqlSuche']"));
            $("input[name='sqlSuche']").on('keyup',function(e){
                e.preventDefault();
                if(e.keyCode === 13){
                    test(e);
                    console.log('test');
                }
            });


            $( window ).scroll(function() {
                localStorage.setItem("positionX", window.scrollX);
                localStorage.setItem("positionY", window.scrollY);
            });

            document.ondragstart = function(event) {
                console.log(event);
            };

            document.ondragend = function(event) {
                $('#spinner').css("display","block");

                console.log(event);
                var Sortierung =document.getElementById('thed').children[0].children[0].id;
                var laenge = document.getElementById('thed').children[0].childElementCount;
                for(var i=1;i<laenge-1;i++){
                    Sortierung += "," + document.getElementById('thed').children[0].children[i].id ;
                }
                console.log(Sortierung);
                $.ajax({
                    type: 'POST',

                    url: 'index.php?cl=[{$oView->getClassName()}]&fnc=sortierung&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
                    data: {
                        query: Sortierung
                    },
                    success: function(result){
                        $('#spinner').css("display","none");

                    },
                    error: function(result){
                        console.log('\nERROR');
                    }
                });
            };

            /**
             *  Testphase für Spaltenbreitenanpassung durch ziehen
             */


            /*
            $("table#defaultTable th").resizable({
                handles: "e",
                minWidth: 20,
                resize: function (event, ui) {
                    var sizerID = "#" + $(event.target) + "-sizer";
                    $(sizerID).width(ui.size.width);
                }
            });
               */
            /**
             *  Testphase für Spaltenbreitenanpassung durch ziehen
             */

            for(var i = 0; i < $("input[name='localCounterSpaltenMin']").val(); i++){
                var localVar = $("input[name='button" + i + "']").val();
                if(localVar === '1'){
                    console.log("AENDERUNG: " + i);
                    var ButtonId = document.getElementById(i);
                    $(ButtonId).click();
                }

                console.log(localVar);
            }


            /*
            $('.table').dragableColumns({
                drag: true,
            dragClass: 'drag',
            overClass: 'over',
            movedContainerSelector: '.dnd-moved'
        }); */

            inPut = false;
            start = document.getElementById('start');



            // start.style.backgroundColor = 'pink';
            //start.style.color = 'white';




            var idPos = localStorage.getItem("position");

            var idPosX = localStorage.getItem("positionX");
            var idPosY = localStorage.getItem("positionY");
            if(idPosX=== "0" && idPosY === "0"){

                setTimeout(function() {
                    $('html').focus();
                }, 0);
                //  start.focus();
            } else {
                window.scrollBy(idPosX, idPosY);
            }


            //start.focus();

            document.onkeydown = checkKey;

            /*  localStorage.setItem("positionX", 0);
              localStorage.setItem("positionY", 0); */


        });

        function dotheneedful(sibling) {

            if (sibling != null) {
                start.focus();
                start.style.backgroundColor = '';
                start.style.color = '';
                sibling.focus();
                sibling.style.backgroundColor = 'pink';
                //sibling.style.color = 'white';
                start = sibling;
            }
        }

        function checkKey(e) {
            e = e || window.event;
            if (e.keyCode == '13') {
                var idA = start.children[0].children[0].children[0];
                if(inPut === true){
                    console.log(e.target);
                    var object = $(e.target).parents("td").find("button");
                    console.log(object[0]);
                    object[0].click();
                    inPut = false;
                } else {

                }
            } else if (e.keyCode == '27'){
                if(inPut === true){
                    var object = $(e.target).parents("td").find("button");
                    console.log(object);
                    object[1].click();
                    inPut = false;
                }
            }
        }

        /**
         * Diese Funktion ist zum exportieren der Tabelle da. Schreibt die Tabelle in eine Textdatei, wobei das Format einer CSV-Datei eingehalten wird.
         */

        [{* TODO: Oxids aus Inputs auslesen, alle Überschriften einlesen. -> AJAX, dann mit SQL Befehl direkt in CSV Datei schreiben  *}]

        function exportCSV(event){
            event.preventDefault();
            var thead = document.getElementById('defaultTable').children[0].children[0].cells;
            var tbod = document.getElementById('defaultTable').children[1].children[0].children;
            var tbod1 = document.getElementById('defaultTable').children[1].children;

            var String = "";

            for(var i=0;i<thead.length-1;i++){
                String += thead[i].id + ",";
            }

            String += thead[thead.length-1].id + "\n";

            for(var k=0;k<tbod1.length-1;k++){
                if(tbod1[k].id != '') {
                    var tbodExtend = tbod1[k].children;
                    for (var j = 0; j < tbod.length - 1; j++) {
                        String += tbodExtend[j].innerText + ",";
                    }
                    String += "\n";
                }
            }

            $.ajax({
                type: 'POST',
                url: 'index.php?cl=[{$oView->getClassName()}]&fnc=csv&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
                data: {
                    csv: String
                },
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                }
            });
        }


        /**
         *  Funktionalität von der article_list.tpl
         */




        /*
        function sortStand(event){
            $('#spinner').css("display","block");

            console.log('Test');
            event.preventDefault();
            $.ajax({
                type: 'get',
                //url: 'index.php?cl=[{$oView->getClassName()}]&fnc=sortStand&stoken=E0B84011&force_admin_sid=tf2p351c5tgv9eka0mik2qduso',
                url: 'index.php?cl=[{$oView->getClassName()}]&fnc=sortStand&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
                data: {
                },
                success: function(result){
                    //console.log(result);
                    location.reload();
                },
                error: function(result){
                    console.log(result + '\nERROR');
                }

            });
        }
        */

        /**
         * Klick auf OK --> Text des Eingabefeldes wird auf das hidden input-Text übergeben. Außerdem wird das Formular abgeschickt.
         */
        /*
        function spinner(){
            $('#spinner').css("display","block");
            console.log("TEST");
            document.getElementById("formSuche").submit();
            console.log("TEST NACH SUBMIT");
        }
        */

        [{* Button wird statt submit button.. die Funktion des submits wird in die funktion spinner gesetzt.*}]

        function test(event){
            event.preventDefault();
            if($('#finaleSuche').length <=0) {
                var SuchButtonFinal = '<button id="finaleSuche" [{*onclick="spinner();"*}] class="btn btn-success btn-xs" type="submit" name="submitit" style="margin-left:30px;" tabindex="6">Suchen</button>';
                $('#finaleSucheContainer').append(SuchButtonFinal);
            }
            var Inhalt = $("input[name='sqlSuche']").val();
            if(Inhalt === '0'){
                Inhalt = '0 ';
            }
            // Get the input field
            var input = document.getElementById("finaleSuche");
            // Execute a function when the user releases a key on the keyboard
            input.addEventListener("keyup", function(event) {
                // Number 13 is the "Enter" key on the keyboard
                if (event.keyCode === 13) {
                    // Cancel the default action, if needed
                    event.preventDefault();
                    // Trigger the button element with a click
                    document.getElementById("finaleSuche").click();
                }
            });
            $("input[name='actPage']").val(0);
            console.log(Inhalt);
            var Spalte = document.getElementById('FeldBezeichner').value;
            var counterNumber = parseInt(document.getElementById('counterSearch').value);
            counterNumber=counterNumber+1;
            document.getElementById('counterSearch').value = counterNumber;
            $("input[name='AnzahlSuche']").val(counterNumber);
            var String = 'suche' + counterNumber;

            var div ='<div class="col-xs-2">' +
                '<div class="input-group">' +
                '<input type="text" class="form-control" value="' + Spalte + " : " + Inhalt + '" disabled >' +
                '<input type="hidden" name='+counterNumber+' value="' + Spalte + '" >' +
                '<input type="hidden" name='+String+' value="' + Inhalt + '" >' +
                '<span class="input-group-btn">' +
                '<button id="delete' + counterNumber + '" class="btn btn-default" onclick="$(this).parents(\'.col-xs-2\').remove();"><span class="fas fa-minus-circle"></button>' +
                '</span>' +
                '</div>' +
                '</div>';

            $('#formSuche').append(div);
            // var TestInput = '<input type="hidden" name=Input" value='+Inhalt+'>';
            // $('#formSuche').append(TestInput);
            //$("input[name="+String+"]").val(Spalte + ' : ' + Inhalt);
            // console.log($("input[name="+String+"]").val());
        }

        /**
         * Replace-All Funktion: Ersetzt alle Werte in der gleichen Spalte mit dem Wert im input-Field.
         */

        function replaceAll(event){

            var check = confirm('Wirklich alles ersetzen?');
            if (check === false) {
                return;
            }

            var String2 = 'input + ' + event.target.id;
            var recover = $("input[name='recover']").val();

            var ErgebnisArray = [];

            //var tar = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.children;
            var j = 1;
            while($("input[name='oxidDelete" + j + "']").val()){
                ErgebnisArray[j-1] = $("input[name='oxidDelete" + (j) + "']").val();
                j++;
            }
            /*
            for(i=0;i<tar.length;i++) {
                ErgebnisArray[i] = $("input[name='oxidDelete" + (i+1) + "']").val();
                //ErgebnisArray[i] = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.children[i].id;
            } */
            console.log(ErgebnisArray);

            var Spalte = document.getElementById(String2).offsetParent.children[1].innerHTML;

            var text = document.getElementById(String2).value;
            $.ajax({
                type: 'POST',
                //url: 'index.php?cl=[{$oView->getClassName()}]&fnc=sortStand&stoken=E0B84011&force_admin_sid=tf2p351c5tgv9eka0mik2qduso',
                url: 'index.php?cl=[{$oView->getClassName()}]&fnc=replaceAll&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
                data: {
                    spalte: Spalte,
                    text: text,
                    array: ErgebnisArray
                },
                success: function(result){
                },
                error: function(result){
                }
            });
            $('.popover-dismiss').popover('hide');
            $('#spinner').css("display","block");

            window.location.reload();
        }



        function success(e){
            $('#spinner').css("display","block");

            //e.preventDefault();
            inPut = false;
            var String2 = 'input + ' + e.target.id;
            $("input[name='text']").val(document.getElementById(String2).value);
            localStorage.setItem("position", String2);
            localStorage.setItem("textPos",document.getElementById(String2).value);
            console.log(document.getElementById(String2).attributes[2].value);

            $("input[name='styleTA']").val(document.getElementById(String2).attributes[2].value);


            localStorage.setItem("positionX", window.scrollX);
            localStorage.setItem("positionY", window.scrollY);



            console.log($("input[name='recover']").val());
            if(($("input[name='text']").val() === undefined && $("input[name='recover']").val() === undefined) ||$("input[name='text']").val() === $("input[name='recover']").val()){
                e.preventDefault();
                inPut = false;
                $('#spinner').css("display","none");

                $('.popover-dismiss').popover('hide');
                return;
            }
            var inputFelder = document.getElementById("pagenaviInputs").children;
            console.log(inputFelder);
            var laenge = document.getElementById("pagenaviInputs").children.length;
            var iter = 1;
            for(var j=4;j<laenge;j++){

                if(j>=(laenge+4)/2){
                    var spalt = '<input type="hidden" name="' + ((((laenge+4)/2)-j)+iter) + '" value="' + inputFelder[j].value + '">';
                    //$('#formSuche').append(spalt);
                    //$('#search').append(spalt);
                    iter=iter+2;
                } else {
                    var such = '<input type="hidden" name="suche' + (j-3) + '" value="' + inputFelder[j].value + '">';
                    // $('#formSuche').append(such);
                    //$('#search').append(such);
                }
            }

            //$("input[name='AnzahlErgebnisse']").val(x.text);


            $('.popover-dismiss').popover('hide');
            //document.getElementById("formSuche").submit();

        }

        function successEmpty(e){
            var String2 = 'input + ' + e.target.id;
            $("input[name='text']").val(document.getElementById(String2).value) ;
            $('.popover-dismiss').popover('hide');

        }

        function loesche(e){
            var deleteID = e.target.id;
            var newString = deleteID.substr(4);
            $("input[name='delete']").val(true);
            $("input[name='oxid']").val(newString);
        }

        /**
         * [TODO] Was passiert bei Klick auf Abbrechen? Überlegung: Gar nichts, Popover wird geschlossen.
         */

        function danger(e){
            $('.popover-dismiss').popover('hide');

        }

        /**
         * Klick auf Tabellen-Eintrag --> oxid , text , Spaltenüberschrift werden den hidden-inputs übergeben.
         */

        function editFNC(e){

            inPut=true;
            var active = document.getElementById('start');
            /*if($(active).activeRow !== true){
                $(active).attr('style','background-color:0');
                $(active.parentElement).attr('style','background-color:0');
            } */

            $(active).attr('id','');

            start = e.target.parentElement.parentElement;
            if($(start).is("div")){
                start = start.parentElement;
            }
            start.focus();
            start.style.backgroundColor = 'pink';
            $(start).attr('id','start');

            var parentStart = start.parentElement;
            console.log(start);
            //$(parentStart).attr('style','max-width:50px;background-color:pink !important;');
            $(parentStart).attr('activeRow',true);



            for(var i = 0; i<parentStart.children.length;i++){
                $(active.parentElement.children[i]).css('background-color:0');
                //$(parentStart.children[i])[0].css('background-color:pink !important;');
                //$(parentStart.children[i])[0].attr('style','max-width:50px;background-color:pink !important;');
                //$(parentStart.children[i])[0].attributes.style = 'max-width:50px;background-color:pink !important'; //NEU
                console.log('echo');
                console.log($(parentStart));
            }
            var spalte = e.target.parentElement.dataset.originalTitle;
            if(spalte === undefined){
                spalte = e.target.dataset.originalTitle;
            }
            console.log(e.target.parentElement.dataset.originalTitle === undefined);
            var object = $(e.target).parents("td").find("textarea").val();
            setTimeout(function() {
                $(e.target).parents("td").find("textarea").focus();
                $(e.target).parents("td").find("textarea").val('').val(object);
            }, 0);

            $("input[name='oxid']").val(e.target.offsetParent.parentElement.id);
            $("input[name='text']").val(e.target.innerText);
            $("input[name='recover']").val(e.target.innerText);
            $("input[name='Spalte']").val(spalte);
            //  $("input[name='Spalte']").val(e.target.attributes[9].value);
            var inputFelder = document.getElementById("pagenaviInputs").children;
            var laenge = document.getElementById("pagenaviInputs").children.length;
            var iter = 1;
            for(var j=4;j<laenge;j++){

                if(j>=(laenge+4)/2){
                    var spalt = '<input type="hidden" name="' + ((((laenge+4)/2)-j)+iter) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(spalt);
                    $('#search').append(spalt);
                    iter=iter+2;
                } else {
                    var such = '<input type="hidden" name="suche' + (j-3) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(such);
                    $('#search').append(such);
                }
            }
            var inhalt = 'success' + e.target.id;
            console.log(inputFelder);

        }

        /**
         * Klick auf das MINUS: Entsprechende Spalte wird kleingemacht, die entsprechenden Elemente bekommen eine Breite zugeordnet
         * und ein overflow-hidden.
         * Klick auf das PLUS: Alle vorher getätigten Änderungen werden rückgängig gemacht.
         */

        function minimize(event){
            event.preventDefault();
            if(event.target.innerHTML === '+'){
                $("input[name='button" + event.target.id + "']").val('0');

                event.target.innerHTML = '-';
                var indexZelle = event.target.parentElement.cellIndex;
                var title = event.target.parentElement;
                title.setAttribute("style","");   // <th> wird angepasst

                title.childNodes[1].setAttribute("onmouseover","");   // <th> wird angepasst
                title.childNodes[1].setAttribute("onmouseout","");   // <th> wird angepasst
                title.childNodes[1].setAttribute("style",""); // <a> wird angepasst
                var tbooooody = event.target.parentElement.parentElement.parentElement.parentElement.children[1].children;
                for(var i = 0;i<tbooooody.length;i++){
                    var tdContent = tbooooody[i].children[indexZelle];
                    tdContent.setAttribute("style","max-width:50px !important"); // <td> wird angepasst
                    tdContent.children[0].setAttribute("style","white-space: nowrap;overflow:hidden !important;"); // <div> wird angepasst
                }
                var tables = document.getElementsByTagName('table');
                for (var i=0; i<tables.length;i++){
                    resizableGrid(tables[i]);
                }

                function resizableGrid(table) {
                    var row = table.getElementsByTagName('tr')[0],
                        cols = row ? row.children : undefined;
                    if (!cols) return;

                    table.style.overflow = 'hidden';

                    var tableHeight = table.offsetHeight;

                    for (var i=0;i<cols.length;i++){
                        var div = createDiv(tableHeight);
                        cols[i].appendChild(div);
                        cols[i].style.position = 'relative';
                        setListeners(div);
                    }

                    function setListeners(div){
                        var pageX,curCol,nxtCol,curColWidth,nxtColWidth;

                        div.addEventListener('mousedown', function (e) {
                            curCol = e.target.parentElement;
                            nxtCol = curCol.nextElementSibling;
                            pageX = e.pageX;

                            var padding = paddingDiff(curCol);

                            curColWidth = curCol.offsetWidth - padding;
                            if (nxtCol)
                                nxtColWidth = nxtCol.offsetWidth - padding;
                        });

                        div.addEventListener('mouseover', function (e) {
                            e.target.style.borderRight = '2px solid #0000ff';
                        })

                        div.addEventListener('mouseout', function (e) {
                            e.target.style.borderRight = '';
                        })

                        document.addEventListener('mousemove', function (e) {
                            if (curCol) {
                                var diffX = e.pageX - pageX;

                                if (nxtCol)
                                    nxtCol.style.width = (nxtColWidth - (diffX))+'px';

                                curCol.style.width = (curColWidth + diffX)+'px';
                            }
                        });

                        document.addEventListener('mouseup', function (e) {
                            curCol = undefined;
                            nxtCol = undefined;
                            pageX = undefined;
                            nxtColWidth = undefined;
                            curColWidth = undefined
                        });
                    }

                    function createDiv(height){
                        var div = document.createElement('div');
                        div.style.top = 0;
                        div.style.right = 0;
                        div.style.width = '5px';
                        div.style.position = 'absolute';
                        div.style.cursor = 'col-resize';
                        div.style.userSelect = 'none';
                        div.style.height = height + 'px';
                        return div;
                    }

                    function paddingDiff(col){

                        if (getStyleVal(col,'box-sizing') == 'border-box'){
                            return 0;
                        }

                        var padLeft = getStyleVal(col,'padding-left');
                        var padRight = getStyleVal(col,'padding-right');
                        return (parseInt(padLeft) + parseInt(padRight));

                    }

                    function getStyleVal(elm,css){
                        return (window.getComputedStyle(elm, null).getPropertyValue(css))
                    }
                };
            } else {
                $("input[name='button" + event.target.id + "']").val('1');

                event.target.innerHTML = '+';
                var indexZelle = event.target.parentElement.cellIndex;
                var title = event.target.parentElement;
                title.setAttribute("style","width:20px");   // <th> wird angepasst
                title.childNodes[1].setAttribute("onmouseover","tooltip(this);");   // <th> wird angepasst
                title.childNodes[1].setAttribute("onmouseout","tooltipOff(this);");   // <th> wird angepasst
                title.childNodes[1].setAttribute("style","white-space:nowrap;width:20px;overflow:hidden;"); // <a> wird angepasst
                var tbooooody = event.target.parentElement.parentElement.parentElement.parentElement.children[1].children;
                for(var i = 0;i<tbooooody.length;i++){
                    var tdContent = tbooooody[i].children[indexZelle];
                    tdContent.setAttribute("style","width:20px"); // <td> wird angepasst
                    tdContent.children[0].setAttribute("style","width:20px;overflow:hidden;"); // <div> wird angepasst
                    tdContent.children[0].children[0].setAttribute("style","white-space:nowrap;width:20px;overflow:hidden;"); // <div> wird angepasst
                }


            }


        }

        function tooltip(a){
            a.setAttribute("style","width:20px; margin-bottom:15px");
        }

        function tooltipOff(a){
            a.setAttribute("style","width:20px;overflow:hidden");
        }

        /**
         *  Funktion zum Speichern der minimierten Spalten. Es werden Input Felder dafür ausgelesen.
         */

        function saveSpalten(event){
            $('#spinner').css("display","block");

            event.preventDefault();
            var ErgebnisArray = [];

            //var tar = event.target.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.children;
            var j = 0;
            while($("input[name='button" + j + "']").val()){
                ErgebnisArray[j] = $("input[name='button" + (j) + "']").val();
                j++;
            }



            $.ajax({
                type: 'POST',
                //url: 'index.php?cl=[{$oView->getClassName()}]&fnc=sortStand&stoken=E0B84011&force_admin_sid=tf2p351c5tgv9eka0mik2qduso',
                url: 'index.php?cl=[{$oView->getClassName()}]&fnc=saveSpalten&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]',
                data: {
                    array: ErgebnisArray
                },
                success: function(result){
                    $('#spinner').css("display","none");

                    console.log(result);
                },
                error: function(result){
                }
            });

        }

        /**
         *  anhand der ID festlegen ob next,previous,last,first!
         */


        function pageNavi(x){
            console.log(x);

            var last = x.parentElement.parentElement.children[0].value;
            var NaviPunkte = x.offsetParent.offsetParent.children[0].children[0].childNodes[5].children;
            var pageNaviBullets = document.getElementById('pagenaviBullets');
            console.log(pageNaviBullets);
            for(var i =0;i<pageNaviBullets.children.length;i++){
                if(pageNaviBullets.children[i].attributes[3].nodeValue === 'pagenavigation pagenavigationactive' ){
                    var Treffer = pageNaviBullets.children[i].firstChild.nodeValue;
                }
            }
            console.log(Treffer);
            var laenge = x.parentNode.children.length;
            var iter = 1;
            for(var j=4;j<laenge;j++){

                if(j>=(laenge+4)/2){
                    var spalt = '<input type="hidden" name="' + ((((laenge+4)/2)-j)+iter) + '" value="' + x.parentNode.children[j].value + '">';
                    $('#formSuche').append(spalt);
                    iter=iter+2;
                } else {
                    var such = '<input type="hidden" name="suche' + (j-3) + '" value="' + x.parentNode.children[j].value + '">';
                    $('#formSuche').append(such);
                }
            }

            var actPage;
            if(x.id === 'nav.first'){
                $("input[name='actPage']").val(0);
                //actPage = '<input type="hidden" name="actPage" value="0">';
            } else if(x.id === 'nav.prev') {
                $("input[name='actPage']").val((Treffer-2));
                //actPage = '<input type="hidden" name="actPage" value="' + (Treffer-2) + '">';
            } else if(x.id === 'nav.next'){
                $("input[name='actPage']").val(Treffer);
                // actPage = '<input type="hidden" name="actPage" value="' + (Treffer) + '">';
            } else {
                $("input[name='actPage']").val((last-1));
                //actPage = '<input type="hidden" name="actPage" value="' + (last-1) + '">';
            }
            // $('#formSuche').append(actPage);
            //$('#search').append(actPage);
            $('#spinner').css("display","block");

            document.getElementById("formSuche").submit();
        }

        function pageNaviBullets(x){
            console.log(x);

            // inputFelder
            //console.log(x.offsetParent.offsetParent.children[0].children[0].cells[2].children);
            console.log(document.getElementById("pagenaviInputs"));
            var inputFelder = document.getElementById("pagenaviInputs").children;
            var laenge = document.getElementById("pagenaviInputs").children.length;

            var iter = 1;
            for(var j=4;j<laenge;j++){
                console.log("j: " + j + "laenge: " + laenge);
                console.log(j>=(laenge+4)/2);
                if(j>=(laenge+4)/2){
                    var spalt = '<input type="hidden" name="' + ((((laenge+4)/2)-j)+iter) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(spalt);
                    iter=iter+2;
                } else {
                    var such = '<input type="hidden" name="suche' + (j-3) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(such);
                }
            }

            // actpage
            var actpage = x.firstChild.nodeValue;
            $("input[name='actPage']").val((actpage-1));
            //var actPageInput = '<input type="hidden" name="actPage" value="' + (actpage-1) + '">';
            //$('#formSuche').append(actPageInput);
            console.log(inputFelder);
            $('#spinner').css("display","block");

            document.getElementById("formSuche").submit();

        }


        /**
         * Diese Funktion gibt dem input-Feld 'AnzahlErgebnisse' den Wert des Dropdown-Buttons. Mit den gespeicherten Suchanfragen wird das Formular erneut abgeschickt.
         */

        function AnzahlErgebnisse(x){
            console.log(x.text);
            console.log(document.getElementById("pagenaviInputs"));
            console.log('index.php?cl=[{$oView->getClassName()}]&fnc=AdminZeilen&stoken=[{$oView->getToken()}]&force_admin_sid=[{$oView->getAdminSid()}]');
            var inputFelder = document.getElementById("pagenaviInputs").children;
            var laenge = document.getElementById("pagenaviInputs").children.length;
            var iter = 1;
            for(var j=4;j<laenge;j++){

                if(j>=(laenge+4)/2){
                    var spalt = '<input type="hidden" name="' + ((((laenge+4)/2)-j)+iter) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(spalt);
                    iter=iter+2;
                } else {
                    var such = '<input type="hidden" name="suche' + (j-3) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(such);
                }
            }

            $("input[name='AnzahlErgebnisse']").val(x.text);
            $('#spinner').css("display","block");

            document.getElementById("formSuche").submit();

        }

        /**
         * Diese Funktion soll die Tabelle, nach Klick auf einen Head, dementsprechend sortieren
         * @constructor
         */

        function AufAbSteigend(x){
            console.log(x.innerText);



            var inputFelder = document.getElementById("pagenaviInputs").children;
            var laenge = document.getElementById("pagenaviInputs").children.length;
            var iter = 1;
            for(var j=4;j<laenge;j++){

                if(j>=(laenge+4)/2){
                    var spalt = '<input type="hidden" name="' + ((((laenge+4)/2)-j)+iter) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(spalt);
                    iter=iter+2;
                } else {
                    var such = '<input type="hidden" name="suche' + (j-3) + '" value="' + inputFelder[j].value + '">';
                    $('#formSuche').append(such);
                }
            }
            var aufab = '<input type="hidden" name="aufab" value="' + x.innerText + '">';
            $('#formSuche').append(aufab);

            if($("input[name='asc']").val() != 'ASC'){
                $("input[name='asc']").val('ASC');
            } else {
                $("input[name='asc']").val('DESC');

            }
            $("input[name='actPage']").val("1");
            $('#spinner').css("display","block");

            document.getElementById("formSuche").submit();
        }




    </script>

    [{/if}]