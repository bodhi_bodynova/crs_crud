<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 2019-03-07
 * Time: 13:58
 */

$sLangName = "Deutsch";
$iLangNr = 0;
$aLang = array(
    'charset'                                  => 'UTF-8 ',
    'CRS_CRUD_MAINMENU'                        => 'Datenbank Admin Bereich',
    'CRS_CRUD_SUBMENU_ARTICLES'                => 'Artikel',
    'CRS_CRUD_SUBMENU_USER'                    => 'User',
    'CRS_CRUD_SUBMENU_ACTIONS2ARTICLE'         => 'Actions2Article',
    'CRS_CRUD_SUBMENU_ACTIONS'                 => 'Aktionen',
    'CRS_CRUD_SUBMENU_ARTEXTENDS'              => 'Artextends',
    'CRS_CRUD_SUBMENU_ATTRIBUTE'               => 'Attribute',
    'CRS_CRUD_SUBMENU_CATEGORIES'              => 'Kategorien',
    'CRS_CRUD_SUBMENU_CONTENTS'                => 'Contents',
    'CRS_CRUD_SUBMENU_DELIVERY'                => 'Delivery',
    'CRS_CRUD_SUBMENU_DELIVERYSET'             => 'Delivery-Set',
    'CRS_CRUD_SUBMENU_OBJECT2SELECT'           => 'Object2Select',
    'CRS_CRUD_SUBMENU_ORDER'                   => 'Order',
    'CRS_CRUD_SUBMENU_PRICE2ARTICLE'           => 'Price2Article',
    'CRS_CRUD_SUBMENU_SELECT'                  => 'Select',
    'GENERAL_OXID'                             => 'OXID',
    'GENERAL_OXPARENTID'                       => 'OXPARENTID'
);