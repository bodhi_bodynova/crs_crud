[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
    [{else}]
    [{assign var="readonly" value=""}]
    [{/if}]

[{assign var = "stoken" value=$oView->getToken()}]
[{assign var = "AdminSid" value=$oView->getAdminSid()}]
[{*$stoken|var_dump}]
[{$AdminSid|var_dump*}]


<form id="formSuche" method="post" type="text" style="z-index:10">

    <input type="hidden" name="oxidSuche" value="">
    <input type="hidden" name="AnzahlSuche" value=>

    <input type="hidden" id="counterSearch" value="0">

    [{include file="_formparams.tpl" cl="crs_crud_user_list"lstrt=$lstrt actedit=$actedit fnc="" language=$actlang editlanguage=$actlang}]

    [{assign var="asc" value=$oView->getAsc()}]
    <input type="hidden" name="asc" value=[{if $asc[0] == null}]"ASC"[{else}][{$asc[0]}][{/if}]>

    [{assign var="ergebnisseZahl" value=$oView->getAnzahlErgebnisse()}]
    <div class="container-fluid" style="width:100%; float:left; margin-top:0;margin-bottom:25px;">
        <div id="finaleSucheContainer" class="b1" style="position:fixed;">

            <div class="find" style="float:left">
                <button class="btn btn-danger btn-xs" onclick="sortStand(event);">Standardsortierung</button>
                [{*	<select name="changelang" class="editinput"
						onChange="Javascript:top.oxid.admin.changeLanguage();">
					[{foreach from=$languages item=lang}]
					<option value="[{$lang->id}]" [{if $lang->selected}]SELECTED[{/if}]>[{$lang->name}]
					</option>
					[{/foreach}]
				</select> *}]

            </div>
            <select id="FeldBezeichner">
                [{assign var="feld" value=$oView->inputField()}]

                [{foreach from=$feld item=item}]
                <option value="[{$item}]">[{$item}]
                </option>
                [{/foreach}]
            </select>
            [{*TODO*}]
            <input class="listedit" type="text" size="25" maxlength="128"
                   name="sqlSuche" value="[{$where.oxarticles.oxshortdesc}]"
                   [{include file="help.tpl" helpid=searchfieldoxshortdesc}]>
            <button class="btn btn-info btn-xs" style="margin-left:-3px;" onclick="test(event);">WeiterSuchen...</button>
            <button class="btn btn-default btn-xs" style="float:right; margin-left:200px;margin-top:4px;" onclick="exportCSV(event);">Export CSV</button>
            <div class="btn-group ">
                <button type="button" class="btn btn-xs btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Zeilen <span class="caret"></span>
                </button>
                <ul style="cursor: pointer" class="dropdown-menu">
                    <li><a onclick="AnzahlErgebnisse(this);">10</a></li>
                    <li><a onclick="AnzahlErgebnisse(this);">25</a></li>
                    <li><a onclick="AnzahlErgebnisse(this);">50</a></li>
                    <li><a onclick="AnzahlErgebnisse(this);">100</a></li>
                    <input type="hidden" name="AnzahlErgebnisse" value=[{$ergebnisseZahl[0]}]>

                </ul>

            </div>
        </div>

    </div>
    [{$ergebnisseZahl[0]|var_dump}]
</form>

<div id="liste" >

    [{* Tabelle wird in einem form erstellt, in diesem werden input Felder für die jeweiligen CRUD Befehle zugewiesen*}]

    <form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
        <input type="hidden" name="oxid" value="">
        <input type="hidden" name="text" value="">
        <input type="hidden" name="Spalte" value="">
        <input type="hidden" name="delete" value="">
        <input type="hidden" name="recover" value="">
        <input type="hidden" name="sortierung" value="0">

        [{include file="_formparams.tpl" cl="crs_crud_user_list" lstrt=$lstrt actedit=$actedit fnc="getAll" language=$actlang editlanguage=$actlang}]
        <table class="table" id="defaultTable" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin-left:25px;">


            [{*
			<colgroup>
				[{block name="admin_article_list_colgroup"}]

				[{assign var="col" value=$oView->search()}]
				[{assign var="colcounter" value=0}]

				[{foreach from=$col[0] item="colCounter"}]
					<col draggable="true" id="[{$colcounter}]">
				[{assign var="colcounter" value =$colcounter+1}]
				[{/foreach}]

				[{/block}]
			</colgroup>
			*}]

            [{* Spracheinstellung, Suchfunktion TODO: Spracheinstellung, Suchfunktion


			<tr class="listitem dnd-moved">

				<td valign="top" class="listfilter" colspan="89" nowrap style="height:32px">

					<div class="b1" style="position:fixed;">
						<div class="find" style="float:left">

							<select name="changelang" class="editinput"
									onChange="Javascript:top.oxid.admin.changeLanguage();">
								[{foreach from=$languages item=lang}]
									<option value="[{$lang->id}]" [{if $lang->selected}]SELECTED[{/if}]>[{$lang->name}]
									</option>
								[{/foreach}]
							</select>
							<input class="listedit" type="submit" name="submitit"
								   value="[{oxmultilang ident="GENERAL_SEARCH"}]"
								   onClick="Javascript:document.search.lstrt.value=0;">
						</div>
						<input class="listedit" type="text" size="25" maxlength="128"
							   name="where[oxarticles][oxshortdesc]" value="[{$where.oxarticles.oxshortdesc}]"
							   [{include file="help.tpl" helpid=searchfieldoxshortdesc}]>
					</div>

				</td>


			</tr>		*}]

            [{* Reihe 1 bzw. Header: erstellt dynamisch soviel Suchfelder, wie es Spalten gibt(ausgenommen OXActive und Mülleimer).  *}]
            [{* TODO: Suchfunktion fixen

			<tr class="listitem dnd-moved">

				[{block name="admin_article_list_filter"}]

				<td valign="top" class="listfilter first" align="right">
					<div class="r1">
						<div class="b1">&nbsp;</div>
					</div>
				</td>

				[{assign var="where" value=$oView->getListFilter()}]
				[{assign var="input" value=$oView->inputField()}]
				[{assign var="zähler" value=0}]

				[{foreach from=$input item="SuchFeld"}]
					[{assign var="zaehler" value = $zaehler+1}]
					[{if $zaehler > 1 }]
						<th valign="top" class="listfilter">
							<div class="rl">
								<div class="b1">
									<input class="listedit" type="text" size="9" maxlength="128"
										   name="where[oxarticles][oxartnum]" value="[{$where.oxarticles.oxartnum}]"	>
								</div>
							</div>
						</th>
					[{/if}]
				[{/foreach}]

				<th class="listfilter"></th>

				[{/block}]
			</tr> *}]

            [{* Überschriften/Titel, werden befüllt aus der Funktion inputField *}]
            <thead id="thed">
            <tr class="dnd-moved">
                [{block name="admin_article_list_sorting"}]

                [{assign var="title" value=$oView->inputField()}]

                [{assign var="oxidZaehler" value=0}]


                [{foreach from=$title item="titels"}]

                [{if $titels == 'oxid'}]
                [{assign var="oxidZaehlerFuerID" value=$oxidZaehler}]
                [{/if}]
                <th id="[{$titels}]" class="drag" draggable="true" >
                    <a onclick="AufAbSteigend(this);" class="listheader" style="cursor:pointer;">
                        [{$titels}]
                    </a>
                    <button class="btn-xs" onclick="minimize(event);">-</button>
                    <input type="hidden" name="AufAb" value="">

                </th>

                [{assign var="oxidZaehler" value=$oxidZaehler+1}]
                [{/foreach}]

                [{/block}]

            </tr>
            </thead>

            [{* Tabelleneinträge, werden dynamisch aus sqlFunction gefüllt. Eine Schleife für die Reihen, eine innere Schleife für alle Einträge der Reihen(Spalten).
			 	Mit Hilfe einer Zählvariablen, die den Wert der aktuellen Seite erhält, wird der richtige Content an der richtigen Stelle ausgegeben.
			 	Die ersten beiden Spalten(OXID,OXPARENTID) sind nicht änderbar, alle anderen werden mit einem Popover versehen.
			 *}]

            <tbody>
            [{assign var="cs" value=$pagenavi->actpage*10-10}]
            [{assign var="blWhite" value=""}]
            [{assign var="_cnt" value=0}]

            [{foreach from=$mylist item=listitem}]




                [{assign var="_cnt" value=$_cnt+1}]
                [{assign var="col" value=$oView->search()}]


                [{assign var="shorttest" value=$col.$cs}]
                [{if $shorttest[$oxidZaehlerFuerID] != null}]



                <tr class="dnd-moved" id="[{$shorttest[$oxidZaehlerFuerID]}]">

                    [{block name="admin_article_list_item"}]
                    [{assign var="Spalte" value = 0}]
                    [{foreach from=$col.$cs item="testi"}]
                    [{assign var="Ueberschrift" value=$Spalte}]
                    [{assign var="Spalte" value= $Spalte+1}]

                    [{if $title.$Ueberschrift == "oxid"}]

                    [{* OXID nicht änderbar*}]

                    <td valign="top" class="[{$listclass}]">
                        <div class="listitemfloating">
                            <div>
                                <span class="maxi">[{$testi}]</span>
                                <span class="mini">[{$testi|oxtruncate:20:"..."}]</span>
                            </div>
                        </div>

                    </td>

                    [{elseif $testi == null}]

                    [{* TODO: Popover bei keinem Inhalt? *}]

                    <td valign="top">
                        <div class="listitemfloating">
                            <a class="popover-dismiss" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" title="[{$title.$Ueberschrift}]"  id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" onclick="editFNC(event)" style="width:50px; height:10px; display:block; cursor:pointer;">
                                <span class="mini">[{$testi|oxtruncate:10:"..."}]</span>
                                <span class="maxi">[{$testi}]</span>
                            </a>
                            <div class="content hide">
                                <div class="form-group">
                                    <textarea id="input + [{$cs+1}] + [{$Spalte}] + [{$testi}]" class="form-control">[{$testi}]</textarea>
                                </div>
                                <div class="row">
                                    <button onclick="successEmpty(event)" id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="submit" class="btn-success" style="margin-left:10px">OK</button>
                                    <button type="button" class="btn-danger" style="margin-right:10px"
                                            onclick="danger(event)">Abbrechen
                                    </button>
                                    <button id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="button" class="btn-primary" style="margin-left:-10px;margin-right: 10px;" onclick="replaceAll(event);">Alles ersetzen</button>
                                </div>
                            </div>
                        </div>
                    </td>

                    [{else}]

                    <td valign="top">
                        <div class="listitemfloating">
                            <a class="popover-dismiss" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" title="[{$title.$Ueberschrift}]"  id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" onclick="editFNC(event)" style="cursor:pointer;">
                                <span class="mini">[{$testi|oxtruncate:10:"..."}]</span>
                                <span class="maxi">[{$testi}]</span>
                            </a>
                            <div class="content hide">
                                <div class="form-group">
                                    <textarea id="input + [{$cs+1}] + [{$Spalte}] + [{$testi}]" class="form-control">[{$testi}]</textarea>
                                </div>
                                <div class="row">
                                    <button onclick="success(event)" id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="submit" class="btn-success" style="margin-left:10px">OK</button>
                                    <button type="button" class="btn-danger" style="margin-right:10px"
                                            onclick="danger(event)">Abbrechen
                                    </button>
                                    <button id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="button" class="btn-primary" style="margin-left:-10px;margin-right: 10px;" onclick="replaceAll(event);">Alles ersetzen</button>
                                </div>
                            </div>
                        </div>
                    </td>

                    [{/if}]

                    [{/foreach}]
                    [{assign var="cs" value = $cs+1}]

                    [{* TODO: Mülleimer mit Löschfunktion*}]

                    <td valign="top">
                        <div class="listitemfloating">

                            [{if !$readonly}]
                            <a class="delete popover-dismiss" data-placement="left" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" id="del.[{$shorttest[$oxidZaehlerFuerID]}]" style="cursor:pointer;"></a>

                            [{/if}]
                            <div class="content hide">
                                <div class="form-group" style="width:150px">
                                    <p>Möchtest du diesen Artikel wirklich löschen?</p>
                                </div>
                                <div class="row">
                                    <button id="del.[{$shorttest[$oxidZaehlerFuerID]}]" onclick="loesche(event)" type="submit" class="btn-success" style="margin-left:10px">OK</button>
                                    <button type="button" class="btn-danger" style="margin-right:10px" onclick="danger(event)">Abbrechen</button>
                                </div>
                            </div>
                        </div>
                    </td>

                    [{/block}]

                </tr>
                [{/if}]
                [{if $blWhite == "2"}]
                [{assign var="blWhite" value=""}]
                [{else}]
                [{assign var="blWhite" value="2"}]
                [{/if}]
                [{/foreach}]
            </tbody>
            [{* PAGE Navigation, übernommen aus entsprechender .tpl Datei. TODO: ein paar Bugs fixen..*}]

            [{block name="pagenavisnippet_main"}]
            [{if $pagenavi}]

            [{assign var="linkSort" value=""}]

            [{foreach from=$oView->getListSorting() item=aField key=sTable}]

            [{foreach from=$aField item=sSorting key=sField}]

            [{assign var="linkSort" value=$linkSort|cat:"sort["|cat:$sTable|cat:"]["|cat:$sField|cat:"]="|cat:$sSorting|cat:"&amp;"}]
            [{/foreach}]
            [{/foreach}]

            [{assign var="where" value=$oView->getListFilter()}]
            [{assign var="whereparam" value="&amp;"}]
            [{foreach from=$where item=aField key=sTable}]
            [{foreach from=$aField item=sFilter key=sField}]
            [{assign var="whereparam" value=$whereparam|cat:"where["|cat:$sTable|cat:"]["|cat:$sField|cat:"]="|cat:$sFilter|cat:"&amp;"}]
            [{/foreach}]
            [{/foreach}]

            [{* $oView->getViewListSize wird auf anderen Wert gesetzt! }]

			[{if $footZaehler != null}]
			[{assign var="footZaehler" value=$footZaehler-1}]
			[{/if}]

			[{$footZaehler|var_dump}]

			[{ $oView->getViewListSize wird auf anderen Wert gesetzt! *}]

            [{assign var="viewListSize" value=$oView->getViewListSize()}]
            [{assign var="whereparam" value=$whereparam|cat:"viewListSize="|cat:$viewListSize}]
            [{*$oView->getViewListSize()|var_dump*}]

            [{assign var="pageNavigationSuche" value=$oView->getSuche()}]
            [{assign var="pageNavigationSpalte" value=$oView->getSpalte()}]
            [{assign var="pageNavigationAnzahl" value=$oView->getAnzahl()}]
            [{*$pageNavigationSuche|var_dump}]
			[{$pageNavigationSpalte|var_dump}]
			[{$pageNavigationAnzahl|var_dump*}]

            [{assign var="pageNaviga" value=$pagenavi->pages}]


            <tr class="dnd-moved">
                <td class="pagination pagenavi" colspan="[{$colspan|default:"13"}]">
                    <div class="r1">
                        <div class="b1">

                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                <tr>
                                    <input type="hidden" value=[{$pageNaviga}]>
                                    <td id="nav.site" class="pagenavigation" align="left" width="33%">
                                        [{oxmultilang ident="NAVIGATION_PAGE"}] [{$pagenavi->actpage}] / [{$pagenavi->pages}]
                                    </td>
                                    <td class="pagenavigation" height="22" align="center" width="33%">
                                        [{foreach key=iPage from=$pagenavi->changePage item=page}]
                                        <a onclick="pageNaviBullets(this)" style="cursor: pointer;" id="nav.page.[{$iPage}]" class="pagenavigation[{if $iPage == $pagenavi->actpage}] pagenavigationactive[{/if}]" hsref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{$iPage}]&amp;[{$linkSort}]actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{$iPage}]</a>

                                        [{/foreach}]
                                    </td>

                                    <td id="pagenaviInputs" class="pagenavigation" align="right" width="33%">
                                        <a onclick="pageNavi(this)" id="nav.first" [{if $pagenavi->actpage == 1}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=1&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_FIRST"}]</a>
                                        <a onclick="pageNavi(this)" id="nav.prev" [{if $pagenavi->actpage == 1}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{if $pagenavi->actpage-1 > 0}][{$pagenavi->actpage-1 > 0}][{else}]1[{/if}]&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_PREV"}]</a>
                                        <a onclick="pageNavi(this)" id="nav.next"  [{if $pagenavi->pages == $pagenavi->actpage}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{if $pagenavi->actpage+1 > $pagenavi->pages}][{$pagenavi->actpage}][{else}][{$pagenavi->actpage+1}][{/if}]&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_NEXT"}]</a>
                                        <a onclick="pageNavi(this)" id="nav.last" [{if $pagenavi->pages == $pagenavi->actpage}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{$pagenavi->pages}]&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_LAST"}]</a>
                                        [{assign var="iCount" value=0}]
                                        [{assign var="jCount" value=0}]
                                        [{foreach from=$pageNavigationSuche item=i}]
                                    <input type="hidden" name=suche[{$iCount}] value="[{$i}]">
                                        [{assign var="iCount" value=$iCount+1}]
                                        [{/foreach }]
                                        [{foreach from=$pageNavigationSpalte item=j}]
                                    <input type="hidden" name=[{$jCount}] value="[{$j}]">
                                        [{assign var="jCount" value=$jCount+1}]
                                        [{/foreach }]

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            [{/if}]
            [{/block}]

        </table>
    </form>
</div>

[{include file="pagetabsnippet.tpl"}]
