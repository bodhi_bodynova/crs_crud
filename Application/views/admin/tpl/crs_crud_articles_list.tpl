[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]

[{if $readonly}]
	[{assign var="readonly" value="readonly disabled"}]
	[{else}]
	[{assign var="readonly" value=""}]
	[{/if}]

[{assign var = "stoken" value=$oView->getToken()}]
[{assign var = "AdminSid" value=$oView->getAdminSid()}]
[{*$stoken|var_dump}]
[{$AdminSid|var_dump*}]

[{assign var="aufUndAb" value=$oView->getAufAb()}]

[{assign var="class" value=$oView->getClassName()}]

[{assign var="pageNavigationSuche" value=$oView->getSuche()}]
[{assign var="pageNavigationSpalte" value=$oView->getSpalte()}]
[{assign var="pageNavigationAnzahl" value=$oView->getAnzahl()}]
[{assign var="actualPage" value=$oView->getActPage()}]


[{assign var="styleTA" value=$oView->getStyleTA()}]
[{assign var="user" value=$oView->getUser()}]
<form id="formSuche" method="post" type="text" style="z-index:10">

	<input type="hidden" name="styleTA" value="[{$styleTA}]">
	<input type="hidden" name="actPage" value="[{$actualPage[0]}]">

	<input type="hidden" name="aufab" value="[{$aufUndAb}]">

	<input type="hidden" name="oxidSuche" value="">
	<input type="hidden" name="AnzahlSuche" value=[{$pageNavigationSuche|@count}]>

	<input type="hidden" id="counterSearch" value=[{$pageNavigationSuche|@count}]>
	[{include file="_formparams.tpl" cl=$class lstrt=$lstrt actedit=$actedit fnc="" language=$actlang editlanguage=$actlang}]
	[{assign var="asc" value=$oView->getAsc()}]
	<input type="hidden" name="asc" value=[{if $asc[0] == null}]"ASC"[{else}][{$asc[0]}][{/if}]>


	[{assign var="ergebnisseZahl" value=$oView->getAnzahlErgebnisse()}]
	[{assign var="feld" value=$oView->inputField()}]

	<div class="container-fluid" style="width:100%; float:left; margin-top:0;margin-bottom:25px;">
		<div id="spinner" class="spinner-border" style="display:none"></div>

		<div id="finaleSucheContainer" class="b1" style="position:fixed;">
			<div class="find" style="float:left">
				<button type="button" class="btn btn-danger btn-xs" onclick="enlarge();" tabindex="1">Sortierung</button>
				<div id="containerSort" class="container pull-left hidden" style="position:absolute;width:200px;max-height:750px;margin-top:47px;padding-bottom:10px;z-index:10000;background-color:lightgrey;overflow-x: scroll" >
					<h3>Choose Your Sortierung</h3><hr>
					<ul id="sortable" style="cursor:pointer">
						[{foreach from=$feld item="key"}]
						<li class="ui-state-default" id=[{$key}]>[{$key}]</li>
						[{/foreach}]
					</ul>
					<hr>
					<button type="button" class="btn btn-success" onclick="saveSort()">Speichern</button>
				</div>
				[{*	<select name="changelang" class="editinput"
						onChange="Javascript:top.oxid.admin.changeLanguage();">
					[{foreach from=$languages item=lang}]
					<option value="[{$lang->id}]" [{if $lang->selected}]SELECTED[{/if}]>[{$lang->name}]
					</option>
					[{/foreach}]
				</select> *}]

			</div>
			<select id="FeldBezeichner" tabindex="2">

				[{foreach from=$feld item=item}]
				<option value="[{$item}]"[{if $pageNavigationSpalte[0] == $item}]selected[{/if}]>[{$item}]
				</option>
				[{/foreach}]
			</select>
			<input tabindex="3" type="text" size="25" maxlength="128"
				   name="sqlSuche" value="" data-toggle="tooltip" data-placement="bottom" title="Für eine exakte Suche den Sucheintrag in einzelne Gänsefüßchen setzen : 'Beispielsuche'">
			<button type="button" class="btn btn-info btn-xs" style="margin-left:-3px;" onclick="test(event);" tabindex="4">WeiterSuchen...</button>
			[{*<button type="button" class="btn btn-default btn-xs" style="background-color:hotpink;float:right; margin-left:2px;margin-top:4px;" onclick="exportCSV(event);">Export CSV</button>
			<button type="button" class="btn btn-default btn-xs" style="background-color:greenyellow;float:right; margin-left:200px;margin-top:4px;" onclick="saveSpalten(event);">Spalten(+/-)</button>*}]

			<div class="btn-group ">
				<button type="button" class="btn btn-xs btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Zeilen <span class="caret"></span>
				</button>
				<ul style="cursor: pointer;margin-top:42px;" class="dropdown-menu">
					<li><a onclick="AnzahlErgebnisse(this);">10</a></li>
					<li><a onclick="AnzahlErgebnisse(this);">25</a></li>
					<li><a onclick="AnzahlErgebnisse(this);">50</a></li>
					<li><a onclick="AnzahlErgebnisse(this);">100</a></li>
					<li><a onclick="AnzahlErgebnisse(this);">1000</a></li>
					<input type="hidden" name="AnzahlErgebnisse" value=[{$ergebnisseZahl[0]}]>
				</ul>

			</div>

		</div>

	</div>

	[{*TODO: Letztes Suchergebnis hinzufügen*}]
	[{assign var="lastSearchSpalte" value=1}]
	[{foreach from=$pageNavigationSuche item=item key=key}]
	<div class="col-xs-2">
		<div class="input-group">
			<input type="text" class="form-control" value="[{$pageNavigationSpalte.$key}] : [{$item}]" disabled >
			<input type="hidden" name="[{$lastSearchSpalte}]" value="[{$pageNavigationSpalte.$key}]" >
			<input type="hidden" name="suche[{$lastSearchSpalte}]" value="[{$item}]" >
			<span class="input-group-btn">
                            <button id="delete[{$lastSearch}]" class="btn btn-default" onclick="$(this).parents('.col-xs-2').remove();"><span class="fas fa-minus-circle"></button>
				</span>
		</div>
	</div>
	[{assign var="lastSearchSpalte" value=$lastSearchSpalte+1}]

	[{/foreach}]
</form>

<div id="liste">
	[{* Tabelle wird in einem form erstellt, in diesem werden input Felder für die jeweiligen CRUD Befehle zugewiesen*}]

	<form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
		<input type="hidden" name="styleTA" value="[{$styleTA}]">

		[{*TODO: Letztes Suchergebnis hinzufügen*}]
		<input type="hidden" name="AnzahlSuche" value=[{$pageNavigationSuche|@count}]>

		[{assign var="lastSearchSpalte" value=1}]

		[{foreach from=$pageNavigationSuche item=item key=key}]

	<input type="hidden" name="[{$lastSearchSpalte}]" value="[{$pageNavigationSpalte.$key}]" >
	<input type="hidden" name="suche[{$lastSearchSpalte}]" value="[{$item}]" >

		[{assign var="lastSearchSpalte" value=$lastSearchSpalte+1}]

		[{/foreach}]
		<input type="hidden" name="aufab" value="[{$aufUndAb}]">
		<input type="hidden" name="asc" value=[{if $asc[0] == null}]"ASC"[{else}][{$asc[0]}][{/if}]>

		<input type="hidden" name="oxid" value="">
		<input type="hidden" name="text" value="">
		<input type="hidden" name="Spalte" value="">
		<input type="hidden" name="delete" value="">
		<input type="hidden" name="recover" value="">
		<input type="hidden" name="sortierung" value="0">

		[{assign var="title" value=$oView->inputField()}]

		[{assign var="oxidZaehler" value=0}]
		[{assign var="SpaltenMin" value=$oView->SpaltenMin()}]
		[{assign var="localCounter" value=0}]
		[{foreach from=$SpaltenMin item="id"}]
	<input type="hidden" name="button[{$localCounter}]" value=[{$id}]>
		[{assign var="localCounter" value=$localCounter+1}]
		[{/foreach}]
		<input type="hidden" name="localCounterSpaltenMin" value=[{$localCounter}]>
		[{include file="_formparams.tpl" cl=$class lstrt=$lstrt actedit=$actedit fnc="getAll" language=$actlang editlanguage=$actlang}]
		<table class="" id="defaultTable" cellspacing="0" cellpadding="0" border="0" style="margin-left:25px; margin-top:60px !important; [{if $class=="crs_crud_articles_list" && ($user->oxuser__oxusername->rawValue =="n.farzanegan@bodynova.de" || $user->oxuser__oxusername->rawValue =="j.fluegel@bodynova.de" || $user->oxuser__oxusername->rawValue =="d.hoffmann@bodynova.de")}]width:2800px !important;[{elseif $class=="crs_crud_articles_list" && $user->oxuser__oxusername->rawValue =="Malik" }]width:6000px !important;[{elseif $class=="crs_crud_articles_list"}]width:12000px;[{else}]width:2500px;[{/if}]">

			[{* Überschriften/Titel, werden befüllt aus der Funktion inputField *}]

			<thead id="thed">
			<tr class="dnd-moved">
				[{block name="admin_article_list_sorting"}]

				[{foreach from=$title item="titels"}]

				[{if $titels == 'oxid' || $titels == 'OXID'}]

				[{assign var="oxidZaehlerFuerID" value=$oxidZaehler}]
				[{/if}]
				<th id="[{$titels}]" class="" draggable="">
					<a onclick="AufAbSteigend(this);" class="listheader" style="cursor:pointer; white-space: nowrap;overflow:hidden;min-width:50px;">
						[{$titels}]
					</a>
					<button id=[{$oxidZaehler}] class="btn-xs" onclick="minimize(event);">-</button>
					<input type="hidden" name="AufAb" value="">
				</th>

				[{assign var="oxidZaehler" value=$oxidZaehler+1}]
				[{/foreach}]
				<th>&nbsp</th>
				[{/block}]

				[{*
					Hier befuelle ich den Buttons entsprechend InputFelder mit 0 und 1, je nachdem ob die Spalte aufgeklappt ist oder klein bleibt.
				*}]

			</tr>
			</thead>

			[{* Tabelleneinträge, werden dynamisch aus sqlFunction gefüllt. Eine Schleife für die Reihen, eine innere Schleife für alle Einträge der Reihen(Spalten).
			 	Mit Hilfe einer Zählvariablen, die den Wert der aktuellen Seite erhält, wird der richtige Content an der richtigen Stelle ausgegeben.
			 	Die ersten beiden Spalten(OXID,OXPARENTID) sind nicht änderbar, alle anderen werden mit einem Popover versehen.
			 *}]





			<tbody>
			[{assign var="cs" value=$pagenavi->actpage*$ergebnisseZahl[0]-$ergebnisseZahl[0]}]
			[{assign var="blWhite" value=""}]
			[{assign var="_cnt" value=0}]







			[{foreach from=$mylist item=listitem}]


				[{assign var="_cnt" value=$_cnt+1}]
				[{assign var="col" value=$oView->search()}]


				[{assign var="shorttest" value=$col.$cs}]



				[{if $shorttest[$oxidZaehlerFuerID] != null}]


				<tr class="dnd-moved" id="[{$shorttest[$oxidZaehlerFuerID]}]">

					[{block name="admin_article_list_item"}]
					[{assign var="Spalte" value = 0}]

					[{foreach from=$col.$cs item="testi"}]
					[{assign var="Ueberschrift" value=$Spalte}]
					[{assign var="Spalte" value= $Spalte+1}]




					[{if $title.$Ueberschrift == "oxid" || $title.$Ueberschrift == "oxparentid" || $title.$Ueberschrift == 'OXID'}]

					[{*if $title.$Ueberschrift == "oxid" || $title.$Ueberschrift == 'OXID'}]
					<input type="hidden" name="oxidDelete[{$_cnt}]" value="[{$testi}]">

				[{/if*}]

					[{* OXID,OXPARENTID nicht änderbar*}]
					<td [{if ($_cnt == 1 && $Spalte == 1)}]id="start"[{/if}] valign="top" class="[{$listclass}]">
						<div class="listitemfloating" style="white-space: nowrap;overflow:hidden !important;">
							<div>
								<span class="maxi" >[{$testi}]</span>
								<span class="mini">[{$testi}]</span>
							</div>
						</div>

					</td>

					[{elseif $testi == null}]

					[{* TODO: Popover bei keinem Inhalt? *}]

					<td [{if ($_cnt == 1 && $Spalte == 1 )}]id="start"[{/if}] valign="top" style="max-width:50px">
						<div class="listitemfloating" style="white-space:nowrap; overflow:hidden !important;">
							<a class="popover-dismiss" [{if $Spalte > $shorttest|@count-3}]data-placement="left"[{/if}] tabindex="0" role="button" data-toggle="popover" data-trigger="focus" title="[{$title.$Ueberschrift}]"  id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" onclick="editFNC(event)" style="width:50px; height:10px; display:block; cursor:pointer;">
								<span class="mini">[{$testi|oxtruncate:10:"..."}]</span>
								<span class="maxi" >[{$testi}]</span>
							</a>
							<div class="content hide">
								<div class="form-group">
									<textarea id="input + [{$cs+1}] + [{$Spalte}] + [{$testi}]" class="form-control" style="[{$styleTA}]">[{$testi}]</textarea>
								</div>
								<div class="row">
									<button onclick="successEmpty(event)" id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="submit" class="btn-success" style="margin-left:10px">OK</button>
									<button type="button" class="btn-danger" style="margin-right:10px"
											onclick="danger(event)">Abbrechen
									</button>
									<button id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="button" class="btn-primary" [{if $pageNavigationSuche == null}] disabled [{/if}] style="[{if $pageNavigationSuche == null}] cursor:not-allowed;[{/if}] margin-left:-10px;margin-right: 10px;" onclick="replaceAll(event);">Alles ersetzen</button>
								</div>
							</div>
						</div>
					</td>

					[{else}]

					<td [{if ($_cnt == 1 && $Spalte == 1 )}]id="start"[{/if}] valign="top" style="max-width:50px">
						<div class="listitemfloating" style="white-space:nowrap;overflow:hidden !important;">
							<a class="popover-dismiss" [{if $Spalte > $shorttest|@count-3}]data-placement="left"[{/if}] tabindex="0" role="button" data-toggle="popover" data-trigger="focus" title="[{$title.$Ueberschrift}]"  id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" onclick="editFNC(event)" style="cursor:pointer;">
								<span class="mini">[{$testi}]</span>
								<span class="maxi">[{$testi}]</span>
							</a>
							<div class="content hide">
								<div class="form-group">
									<textarea id="input + [{$cs+1}] + [{$Spalte}] + [{$testi}]" class="form-control" style="[{$styleTA}]">[{$testi}]</textarea>
								</div>
								<div class="row">
									<button onclick="success(event)" id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="submit" class="btn-success" style="margin-left:10px">OK</button>
									<button type="button" class="btn-danger" style="margin-right:10px"
											onclick="danger(event)">Abbrechen
									</button>
									<button id="[{$cs+1}] + [{$Spalte}] + [{$testi}]" type="button" class="btn-primary" [{if $pageNavigationSuche == null}] disabled [{/if}] style="[{if $pageNavigationSuche == null}] cursor:not-allowed;[{/if}] margin-left:-10px;margin-right: 10px;" onclick="replaceAll(event);">Alles ersetzen</button>
								</div>
							</div>
						</div>
					</td>

					[{/if}]

					[{/foreach}]
					[{assign var="cs" value = $cs+1}]

					[{* TODO: Mülleimer mit Löschfunktion*}]

					<td valign="top">
						<div class="listitemfloating">

							[{if !$readonly}]
						<a class="delete popover-dismiss" data-placement="left" tabindex="0" role="button" data-toggle="popover" data-trigger="focus" id="del.[{$shorttest[$oxidZaehlerFuerID]}]" style="cursor:pointer;float:none""></a>

							[{/if}]
							<div class="content hide" style="width:150px">
								<div class="form-group" >
									<p>Möchtest du diesen Eintrag wirklich löschen?</p>
								</div>
								<div class="row">
									<button id="del.[{$shorttest[$oxidZaehlerFuerID]}]" onclick="loesche(event)" type="submit" class="btn-success" style="margin-left:10px;">OK</button>
									<button type="button" class="btn-danger" style="margin-right:10px" onclick="danger(event)">Abbrechen</button>
								</div>
							</div>
						</div>
					</td>

					[{/block}]

				</tr>
				[{/if}]
				[{if $blWhite == "2"}]
				[{assign var="blWhite" value=""}]
				[{else}]
				[{assign var="blWhite" value="2"}]
				[{/if}]
				[{/foreach}]
			</tbody>

			[{* PAGE Navigation, übernommen aus entsprechender .tpl Datei. TODO: ein paar Bugs fixen..*}]

			[{block name="pagenavisnippet_main"}]
			[{if $pagenavi}]

			[{assign var="linkSort" value=""}]

			[{foreach from=$oView->getListSorting() item=aField key=sTable}]

			[{foreach from=$aField item=sSorting key=sField}]

			[{assign var="linkSort" value=$linkSort|cat:"sort["|cat:$sTable|cat:"]["|cat:$sField|cat:"]="|cat:$sSorting|cat:"&amp;"}]
			[{/foreach}]
			[{/foreach}]

			[{assign var="where" value=$oView->getListFilter()}]
			[{assign var="whereparam" value="&amp;"}]
			[{foreach from=$where item=aField key=sTable}]
			[{foreach from=$aField item=sFilter key=sField}]
			[{assign var="whereparam" value=$whereparam|cat:"where["|cat:$sTable|cat:"]["|cat:$sField|cat:"]="|cat:$sFilter|cat:"&amp;"}]
			[{/foreach}]
			[{/foreach}]

			[{* $oView->getViewListSize wird auf anderen Wert gesetzt! }]

			[{if $footZaehler != null}]
			[{assign var="footZaehler" value=$footZaehler-1}]
			[{/if}]

			[{$footZaehler|var_dump}]

			[{ $oView->getViewListSize wird auf anderen Wert gesetzt! *}]

			[{assign var="viewListSize" value=$oView->getViewListSize()}]
			[{assign var="whereparam" value=$whereparam|cat:"viewListSize="|cat:$viewListSize}]


			[{*$pageNavigationSuche|var_dump*}]
			[{*$pageNavigationSpalte|var_dump}]
			[{$pageNavigationAnzahl|var_dump*}]

			[{assign var="pageNaviga" value=$pagenavi->pages}]

			<tr class="dnd-moved">
				<td class="pagination pagenavi" colspan="[{$colspan|default:"13"}]">
					<div class="r1">
						<div class="b1">

							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<input type="hidden" value=[{$pageNaviga}]>
									<td id="nav.site" class="pagenavigation" align="left" width="10%">
										[{oxmultilang ident="NAVIGATION_PAGE"}] [{$pagenavi->actpage}] / [{$pagenavi->pages}]
									</td>
									<td class="pagenavigation" align="left" width="23%">
										[{$col|@count}] Ergebnis[{if $col|@count != 1}]se[{/if}]
									</td>
									<td id="pagenaviBullets" class="pagenavigation" height="22" align="center" width="33%">
										[{foreach key=iPage from=$pagenavi->changePage item=page}]
										<a onclick="pageNaviBullets(this)" style="cursor: pointer;" id="nav.page.[{$iPage}]" class="pagenavigation[{if $iPage == $pagenavi->actpage}] pagenavigationactive[{/if}]" hsref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{$iPage}]&amp;[{$linkSort}]actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{$iPage}]</a>

										[{/foreach}]
									</td>

									<td id="pagenaviInputs" class="pagenavigation" align="right" width="33%">
										<a onclick="pageNavi(this)" id="nav.first" [{if $pagenavi->actpage == 1}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=1&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_FIRST"}]</a>
										<a onclick="pageNavi(this)" id="nav.prev" [{if $pagenavi->actpage == 1}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{if $pagenavi->actpage-1 > 0}][{$pagenavi->actpage-1 > 0}][{else}]1[{/if}]&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_PREV"}]</a>
										<a onclick="pageNavi(this)" id="nav.next"  [{if $pagenavi->pages == $pagenavi->actpage}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{if $pagenavi->actpage+1 > $pagenavi->pages}][{$pagenavi->actpage}][{else}][{$pagenavi->actpage+1}][{/if}]&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_NEXT"}]</a>
										<a onclick="pageNavi(this)" id="nav.last" [{if $pagenavi->pages == $pagenavi->actpage}] style="display:none;cursor:pointer" [{else}] style="cursor:pointer" [{/if}] class="pagenavigation" shref="[{$oViewConf->getSelfLink()}]&cl=[{$oViewConf->getActiveClassName()}]&amp;oxid=[{$oxid}]&amp;jumppage=[{$pagenavi->pages}]&amp;[{$linkSort}]&amp;actedit=[{$actedit}]&amp;language=[{$actlang}]&amp;editlanguage=[{$actlang}][{$whereparam}]&amp;folder=[{$folder}]&amp;pwrsearchfld=[{$pwrsearchfld}]&amp;art_category=[{$art_category}]">[{oxmultilang ident="GENERAL_LIST_LAST"}]</a>
										[{*assign var="iCount" value=0}]
										[{assign var="jCount" value=0}]
										[{foreach from=$pageNavigationSuche item=i}]
											<input type="hidden" name=suche[{$iCount}] value="[{$i}]">
											[{assign var="iCount" value=$iCount+1}]
										[{/foreach }]
										[{foreach from=$pageNavigationSpalte item=j}]
											<input type="hidden" name=[{$jCount}] value="[{$j}]">
											[{assign var="jCount" value=$jCount+1}]
										[{/foreach *}]

										[{*TODO: Letztes Suchergebnis hinzufügen}]
										[{assign var="lastSearch" value=0}]
										[{assign var="lastSearchSpalte" value=1}]

										[{foreach from=$pageNavigationSuche item=item}]

												<input type="hidden" name="[{$lastSearchSpalte}]" value="[{$pageNavigationSpalte.$lastSearch}]" >
												<input type="hidden" name="suche[{$lastSearchSpalte}]" value="[{$item}]" >

										[{assign var="lastSearch" value=$lastSearch+1}]
										[{assign var="lastSearchSpalte" value=$lastSearchSpalte+1}]

										[{/foreach*}]
									</td>
								</tr>
							</table>
						</div>
					</div>
				</td>
			</tr>
			[{/if}]
			[{/block}]

		</table>
		[{assign var="searchOxidDelete" value=$oView->search()}]

		[{* speichere Oxids für Alle Ersetzen*}]
		[{if $pageNavigationSuche != null}]
		[{assign var="zaehler" value=0}]
		[{assign var="_count" value=0}]
		[{foreach from=$searchOxidDelete item=listitem}]
		[{assign var="_count" value=$_count+1}]
		[{assign var="_shorttest" value=$searchOxidDelete.$zaehler}]
		[{assign var="_Spalte" value=0}]

		[{foreach from=$_shorttest item="_testi"}]
		[{assign var="_Ueberschrift" value=$_Spalte}]
		[{assign var="_Spalte" value= $_Spalte+1}]
		[{if $title.$_Ueberschrift == "oxid" || $title.$_Ueberschrift == "OXID"}]
	<input type="hidden" name="oxidDelete[{$_count}]" value="[{$_testi}]">
		[{/if}]

		[{/foreach}]
		[{assign var="zaehler" value=$zaehler+1}]
		[{/foreach}]

		[{/if}]
		[{* speichere Oxids für Alle Ersetzen*}]



		<input type="hidden" name="actPage" value="[{$actualPage[0]}]">
	</form>
</div>
<script type="text/javascript">
	//var tables = document.getElementsByClassName('flexiCol');
	var tables = document.getElementsByTagName('table');
	for (var i=0; i<tables.length;i++){
		resizableGrid(tables[i]);
	}

	function resizableGrid(table) {
		var row = table.getElementsByTagName('tr')[0],
				cols = row ? row.children : undefined;
		if (!cols) return;

		table.style.overflow = 'hidden';

		var tableHeight = table.offsetHeight;

		for (var i=0;i<cols.length;i++){
			if(cols[i].attributes[1].value === ""){
				var div = createDiv(tableHeight);
				cols[i].appendChild(div);
				cols[i].style.position = 'relative';
				setListeners(div);
			}
		}

		function setListeners(div){
			var pageX,curCol,nxtCol,curColWidth,nxtColWidth;

			div.addEventListener('mousedown', function (e) {
				curCol = e.target.parentElement;
				nxtCol = curCol.nextElementSibling;
				pageX = e.pageX;

				var padding = paddingDiff(curCol);

				curColWidth = curCol.offsetWidth - padding;
				if (nxtCol)
					nxtColWidth = nxtCol.offsetWidth - padding;
			});

			div.addEventListener('mouseover', function (e) {
				e.target.style.borderRight = '2px solid #0000ff';
			})

			div.addEventListener('mouseout', function (e) {
				e.target.style.borderRight = '';
			})

			document.addEventListener('mousemove', function (e) {
				if (curCol) {
					var diffX = e.pageX - pageX;

					if (nxtCol)
						nxtCol.style.width = (nxtColWidth - (diffX))+'px';

					curCol.style.width = (curColWidth + diffX)+'px';
				}
			});

			document.addEventListener('mouseup', function (e) {
				curCol = undefined;
				nxtCol = undefined;
				pageX = undefined;
				nxtColWidth = undefined;
				curColWidth = undefined
			});
		}

		function createDiv(height){
			var div = document.createElement('div');
			div.style.top = 0;
			div.style.right = 0;
			div.style.width = '5px';
			div.style.position = 'absolute';
			div.style.cursor = 'col-resize';
			div.style.userSelect = 'none';
			div.style.height = height + 'px';
			return div;
		}

		function paddingDiff(col){

			if (getStyleVal(col,'box-sizing') == 'border-box'){
				return 0;
			}

			var padLeft = getStyleVal(col,'padding-left');
			var padRight = getStyleVal(col,'padding-right');
			return (parseInt(padLeft) + parseInt(padRight));

		}

		function getStyleVal(elm,css){
			return (window.getComputedStyle(elm, null).getPropertyValue(css))
		}
	};
</script>
[{include file="pagetabsnippet.tpl"}]
