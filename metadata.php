<?php
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'crs_crud',
    'title'       => [
        'en' => '<img src="../modules/ewald/crs_crud/out/img/favicon.ico" title="Visual CMS">odynova Modul zur Datenbankverwaltung',
        'de' => '<img src="../modules/ewald/crs_crud/out/img/favicon.ico" title="Visual CMS">odynova Module for datatable ',
    ],
    'description' => [
        'en' => 'Module for datatable',
        'de' => 'Modul für Datenbankverwaltung',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'c.ewald@bodynova.de',
    'controllers'       => [
        'crs_crud_adminlistcontroller' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_adminlistcontroller::class,
        'crs_crud_majorlistcontroller' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_majorlistcontroller::class,
        'crs_crud_articles_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_articles_list::class,
        'crs_crud_user_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_user_list::class,
        /*
        'crs_crud_actions2article_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_actions2article_list::class,
        */
        'crs_crud_actions_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_actions_list::class,
        'crs_crud_artextends_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_artextends_list::class,
        'crs_crud_attribute_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_attribute_list::class,
        'crs_crud_categories_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_categories_list::class,
        /*
        'crs_crud_contents_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_contents_list::class,
        */
        'crs_crud_delivery_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_delivery_list::class,
        'crs_crud_deliveryset_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_deliveryset_list::class,
        'crs_crud_object2select_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_object2select_list::class,
        'crs_crud_order_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_order_list::class,
        'crs_crud_price2article_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_price2article_list::class,
        'crs_crud_select_list' =>
            \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_select_list::class
    ],
    'extend'      => [
        /*
        \OxidEsales\Eshop\Application\Controller\Admin\ArticleList::class =>
        \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_articles_list::class
        */
    ],
    'templates'   => [
        'crs_crud_articles_list.tpl'     => 'ewald/crs_crud/Application/views/admin/tpl/crs_crud_articles_list.tpl',
        'crs_crud_user_list.tpl'         => 'ewald/crs_crud/Application/views/admin/tpl/crs_crud_user_list.tpl'
    ],
    'blocks'      => [
        array(
            'template' => 'headitem.tpl',
            'block'=>'admin_headitem_inccss',
            'file'=>'Application/views/admin/blocks/crs_crud_admin_headitem_inccss.tpl'
        ),
        array(
            'template' => 'headitem.tpl',
            'block'=>'admin_headitem_incjs',
            'file'=>'Application/views/admin/blocks/crs_crud_admin_headitem_incjs.tpl'
        )
    ],
    'settings'    => [
    ],
    'events' => [
        /*
        'onActivate' =>
            'Ewald\VisualCMSWidget\Core\Events::onActivate',
        'onDeactivate' =>
            'Ewald\VisualCMSWidget\Core\Events::onDeactivate',
        */
    ],
];